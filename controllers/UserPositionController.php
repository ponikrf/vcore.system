<?php

namespace system\controllers;

use system\components\Scenario;
use system\components\UserControl;
use system\models\UserPosition;


class UserPositionController extends BaseObjectController {

    public $Model;
    public $objectName = 'Должность';
    public $objectsName = 'Должности';
    public $readOnly = FALSE;    
    public $listRoute = ['/system/user-position/list'];
    public $baseRoute = '/system/user-position/';
    
    public function init() {
        $this->Model = new UserPosition();
        $this->Template->setTemplate('general');
        
        UserControl::setEntity('system', 'user_position');
        
        parent::init();
    }

    public function getModel($id) {
        if (!$Model = UserPosition::find()->where(['id' => $id])->one()) {
            Scenario::run('information', ['objectNotFound']);
        }
        return $Model;
    }

}
