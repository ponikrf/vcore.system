<?php

namespace system\controllers;

use \ext\web\Controller;
use \system\helpers\StringHelper;
use \system\helpers\MoneyHelper;

class DefaultController extends Controller {

    public function actions() {

        $this->layout = '@system/templates/error/layout';

        return [
            'error' => [
                'class' => '\yii\web\ErrorAction',
                'view' => '@system/views/default/error',
            ],
        ];
    }

    public function actionIndex() {
        $this->Template->setTemplate('general');
        return $this->Template->renderView('/default/index', []);
    }
    
    public function actionCreateBaseTable(){
        UserControl::protectAction('add');

        $this->Template->BreadCrumbs->add(['label' => 'Создание базовой таблицы']);
        
        $Model = new \system\forms\BaseTable();
        
        if ($Model->load($this->Request->post()) AND $Model->validate()) {
            $DB = \Yii::$app->db;    
           
            $tableScheme = $Model->scheme;
            $tableName = $Model->table;
            $tableDescription = $Model->description;
            
            $DB->createCommand("
            CREATE TABLE $tableScheme.$tableName
            (
              id serial NOT NULL,
              name character varying(50),
              name_lang character varying(255) NOT NULL,
              name_short character varying(50),
              description text,
              CONSTRAINT {$tableScheme}_{$tableName}_pkey PRIMARY KEY (id)
            )
            ")->queryScalar();

            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name IS 'Название';")->queryScalar();
            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_lang IS 'Читаемое название';")->queryScalar();
            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_short IS 'Короткое название';")->queryScalar();
            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.description IS 'Описание';")->queryScalar();


            $DB->createCommand("COMMENT ON TABLE $tableScheme.$tableName IS '{$tableDescription}';")->queryScalar();            
            
            \system\components\Alert::success("Успешное добавление таблицы");
            Scenario::run('refresh');
        }

        return $this->Template->renderView($this->viewDirectory . 'add', ['Model' => $Model]);
    }
    
    
    public function registers() {
        $registers = $this->Register->getList();
        foreach ($registers as $registerName) {
            echo "<pre>";
            echo "$registerName ";
            print_r($this->Register->get($registerName)->getKeyList());
            echo "</pre>";
        }
    }

    public function actionUpdateTable(){
        $DB = \Yii::$app->db;
        
        $tableScheme = "system";
        $tableName = "application";
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name IS 'Название';")->queryScalar();
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_lang IS 'Читаемое название';")->queryScalar();
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_short IS 'Короткое название';")->queryScalar();
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.description IS 'Описание';")->queryScalar();
        \system\components\Scenario::run('information',['success']);
    }    
    
    public function textEntityTest() {
        $entity = \system\components\TextEntity::createEntity('system', ['id' => '23']);
        StringHelper::ln("Create entity - " . $entity);
        StringHelper::ln(\system\components\TextEntity::replace("Test for system default entity - $entity replased text"));
    }

    public function moneyTest() {
        StringHelper::ln("Money test");
        StringHelper::ln("Convert 1000 penny");
        StringHelper::ln(MoneyHelper::toMoney(1000));

        StringHelper::ln("Convert 10.00 money");
        StringHelper::ln(MoneyHelper::toPenny('10.00'));

        StringHelper::ln("Convert 15 penny");
        StringHelper::ln(MoneyHelper::toMoney(15));

        StringHelper::ln("Convert 15.15 money");
        StringHelper::ln(MoneyHelper::toPenny('15.15'));
    }

}
