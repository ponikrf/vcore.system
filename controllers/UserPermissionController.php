<?php

namespace system\controllers;

use ext\web\Controller;
use system\models\UserRole;
use system\models\UserPermission;
use system\forms\BaseObject;
use yii\data\ActiveDataProvider;
use \system\components\UserControl;


class UserPermissionController extends Controller {

    public function init() {
        $this->Template->setTemplate('general');
        $this->Template->BreadCrumbs->add(['label' => 'Разрешения', 'url' => ['list']]);
        UserControl::setEntity('system','user_permission');
        UserControl::protectAction('show');
        parent::init();
    }

    public function actionList() {
        $this->Template->BreadCrumbs->add(['label' => 'Выбор роли']);
        $Query = UserRole::find()->orderBy('name_lang');

        $dataProvider = new ActiveDataProvider([
            'query' => $Query,
        ]);

        return $this->Template->renderView('/user-permission/role-list', ['dataProvider' => $dataProvider]);
    }

    public function actionSelectModule($role) {

        $RoleModel = $this->getRole($role);

        $this->Template->BreadCrumbs->add(['label' => $RoleModel->name_lang, 'url' => ['select-module', 'role' => $RoleModel->id]]);
        $this->Template->BreadCrumbs->add(['label' => 'Выбор модуля']);

        $accessEntityArray = $this->Register->AccessEntity->getArray();

        $Objects = [];

        foreach ($accessEntityArray as $module => $AccessEntity) {
            $Object = new BaseObject;
            $Object->id = $module;
            $Object->name = $module;
            $Object->name_lang = $AccessEntity['label'];

            $entitys = [];
            foreach ($AccessEntity['entitys'] as $entity) {
                $entitys[] = '[ ' . $entity['label'] . ' ]';
            }

            $Object->description = \system\helpers\ArrayHelper::toString($entitys, ' ');
            $Objects[] = $Object;
        }

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $Objects,
        ]);

        return $this->Template->renderView('/user-permission/module-list', [
                    'dataProvider' => $dataProvider,
                    'role' => $role
        ]);
    }

    public function actionPermission($role, $module) {
        $RoleModel = $this->getRole($role);

        $accessEntityArray = $this->Register->AccessEntity->getArray();
        $AccessEntity = $accessEntityArray[$module];

        $this->Template->BreadCrumbs->add(['label' => $RoleModel->name_lang, 'url' => ['select-module', 'role' => $RoleModel->id]]);
        $this->Template->BreadCrumbs->add(['label' => $AccessEntity['label'], 'url' => ['permission', 'role' => $RoleModel->id, 'module' => $module]]);


        $actions = $AccessEntity['actions'];
        $modelKeys = [];
        $rules = [];
        $checkBoxArray = [];
        foreach ($AccessEntity['entitys'] as $Key => $entity) {

            $modelKeys[$Key] = $entity['label'];
            $rules[] = $Key;
            $entityActions = [];
            foreach ($entity['actions'] as $value) {
                $entityActions[$value] = $actions[$value];
            }

            $checkBoxArray[$Key] = $entityActions;
        }

        $Model = new \yii\base\DynamicModel($modelKeys);
        $Model->addRule($rules, 'safe');

        if ($Model->load($this->Request->post())) {
            
            UserControl::protectAction('edit');
            
            foreach ($AccessEntity['entitys'] as $Key => $entity) {
                UserPermission::deleteAll(['module' => $module, 'entity' => $Key, 'id_user_role' => $role]);
                $AccessArray = $Model->$Key;
                if (!empty($AccessArray)) {
                    foreach ($AccessArray as $action) {
                        $NewPermission = new UserPermission;
                        $NewPermission->id_user = $this->User->getId();
                        $NewPermission->id_user_role = $role;
                        $NewPermission->module = $module;
                        $NewPermission->entity = $Key;
                        $NewPermission->action = $action;
                        \system\components\Notify::dbError($NewPermission->insert(), $NewPermission);
                    }
                }
            }
            
            \system\components\Alert::info('Успешное обновление разрешений');
            \system\components\Scenario::run('toRoute',[['permission', 'role' => $RoleModel->id, 'module' => $module]]);
            
        } else {
            foreach ($AccessEntity['entitys'] as $Key => $entity) {
                $Permissions = UserPermission::find()->where(['module' => $module, 'entity' => $Key, 'id_user_role' => $role])->all();
                if (!empty($Permissions)) {
                    $valuesArray = [];
                    foreach ($Permissions as $Permission) {
                        $valuesArray[] = $Permission->action;
                    }
                    $Model->$Key = $valuesArray;
                }
            }
        }

        return $this->Template->renderView('/user-permission/permission', [
                    'modelKeys' => $modelKeys,
                    'checkBoxArray' => $checkBoxArray,
                    'Model' => $Model,
                    'moduleLabel' => $AccessEntity['label'],
        ]);
    }

    public function getRole($role) {
        if (!$Model = UserRole::find()->where(['id' => $role])->one()) {
            Scenario::run('information', ['objectNotFound']);
        }
        return $Model;
    }

}
