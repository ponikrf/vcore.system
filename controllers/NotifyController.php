<?php

namespace system\controllers;

use \ext\web\Controller;
use \system\helpers\StringHelper;
use \system\helpers\MoneyHelper;

class NotifyController extends Controller {

    public function init() {
        parent::init();
        $this->Template->setTemplate('general');
        UserControl::setEntity('system', 'notify');
        $this->Template->title = 'Оповещения';
    }
}
