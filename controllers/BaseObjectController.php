<?php

namespace system\controllers;

use \ext\web\Controller;
use \system\components\Notify;
use \system\components\Scenario;
use \system\components\UserControl;

use \yii\data\ActiveDataProvider;

class BaseObjectController extends Controller {

    public $Model;
    public $objectName = 'Объект';
    public $objectsName = 'Объекты';    
    public $readOnly = FALSE;
    public $viewDirectory = '@system/views/base-object/';
    public $listRoute = [''];
    public $baseRoute = '/system/user-role/';    
    public $orderBy = 'name_lang';
    
    public function init() {
        parent::init();
        
        $this->Template->addViewParams([
            'objectName' => $this->objectName,
            'objectsName' => $this->objectsName,
            'listRoute' => $this->listRoute,
            'baseRoute' => $this->baseRoute,
            'readOnly' => $this->readOnly,
        ]);
        
        $this->Template->BreadCrumbs->add(['label' => $this->objectsName,'url'=>  $this->listRoute]);
    }
    
    public function actionList() {
        
        UserControl::protectAction('show');
        
        $Query = $this->Model->find()->orderBy($this->orderBy);

        $dataProvider = new ActiveDataProvider([
            'query' => $Query,
        ]);
        
        return $this->Template->renderView($this->viewDirectory . 'list', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionAdd() {
        
        UserControl::protectAction('add');
        
        if ($this->readOnly)
            Scenario::run ('information',['objectProtection']);
        
        $this->Template->BreadCrumbs->add(['label' => 'Добавить']);
        
        $Model = $this->Model;
        
        if ($Model->load($this->Request->post()) AND $Model->validate()) {
            Notify::dbError($Model->insert(), $Model);
            Scenario::run('toRoute', [$this->listRoute]);
        }

        return $this->Template->renderView($this->viewDirectory . 'add', ['Model' => $Model]);
    }

    public function actionShow($id) {
        
        UserControl::protectAction('show');
        
        $Model = $this->getModel($id);
       
        $this->Template->BreadCrumbs->add(['label' => $Model->name_lang,'url'=>  $this->Route->make('show',['id'=>$Model->id])]);
        
        return $this->Template->renderView($this->viewDirectory . 'show', ['Model' => $Model]);
    }

    public function actionEdit($id) {
        
        UserControl::protectAction('edit');
        
        if ($this->readOnly)
            Scenario::run ('information',['objectProtection']);
        
        $Model = $this->getModel($id);
        
        $this->Template->BreadCrumbs->add(['label' => $Model->name_lang,'url'=>  $this->Route->make('show',['id'=>$Model->id])]);
        $this->Template->BreadCrumbs->add(['label' => 'Редактирование']);
        
        if ($Model->load($this->Request->post()) AND $Model->validate()) {
            Notify::dbError($Model->save(), $Model);
            Scenario::run('toRoute', [$this->listRoute]);
        }
        return $this->Template->renderView($this->viewDirectory . 'edit', ['Model' => $Model]);
    }

    public function actionDelete($id) {
        
        UserControl::protectAction('delete');
        
        if ($this->readOnly)
            Scenario::run ('information',['objectProtection']);
        
        $Model = $this->getModel($id);
        Notify::dbError($Model->delete(), $Model);
        Scenario::run('toRoute', [$this->listRoute]);
    }

    public function getModel($id) {
        
    }

}
