<?php

namespace system\controllers;

use system\components\Scenario;
use system\components\UserControl;
use system\models\UserRole;


class UserRoleController extends BaseObjectController {

    public $Model;
    public $objectName = 'Роль';
    public $objectsName = 'Роли';
    public $readOnly = FALSE;    
    public $listRoute = ['/system/user-role/list'];
    public $baseRoute = '/system/user-role/';
    
    public function init() {
        $this->Model = new UserRole();
        $this->Template->setTemplate('general');
        
        UserControl::setEntity('system', 'user_role');
        
        parent::init();
    }

    public function getModel($id) {
        if (!$Model = UserRole::find()->where(['id' => $id])->one()) {
            Scenario::run('information', ['objectNotFound']);
        }
        return $Model;
    }

}
