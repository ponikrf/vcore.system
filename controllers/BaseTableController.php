<?php

namespace system\controllers;

use ext\web\Controller;
use system\components\UserControl;
use system\components\Alert;
use system\components\Scenario;

class BaseTableController extends Controller {

    public function init() {
        parent::init();
        $this->Template->setTemplate('general');
        $this->Template->title = 'Базовые таблицы';
        UserControl::setEntity('system', 'base_table');
    }

    public function actionIndex() {
        
        return $this->Template->renderHtml('', []);
    }

    public function actionReferenceTable() {

        $this->Template->leftContent = \yii\bootstrap\Nav::widget([
            'options'=>['class'=>'nav nav-list'],
            'items' => [
                \yii\helpers\Html::tag('li', 'Базовые таблицы', ['class'=>'nav-header']),
                ['label'=>'Справочная таблица','url'=>['/system/base-table/reference-table']],
            ],
        ]);
        

                
      #  UserControl::protectAction('add');

        $this->Template->BreadCrumbs->add(['label' => 'Создание справочной таблицы']);

        $Model = new \system\forms\BaseTable();

        if ($Model->load($this->Request->post()) AND $Model->validate()) {
            $DB = \Yii::$app->db;

            $tableScheme = $Model->scheme;
            $tableName = $Model->table;
            $tableDescription = $Model->description;

            $DB->createCommand("
            CREATE TABLE $tableName
            (
              id int(11) NOT NULL AUTO_INCREMENT,
              name character varying(50) COMMENT 'Название',
              name_lang character varying(255) NOT NULL COMMENT 'Читаемое название',
              name_short character varying(50) COMMENT 'Короткое название',
              description text COMMENT 'Описание',
              PRIMARY KEY  (id)
            )
            ")->queryScalar();
/*
            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name IS 'Название';")->queryScalar();
            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_lang IS 'Читаемое название';")->queryScalar();
            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_short IS 'Короткое название';")->queryScalar();
            $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.description IS 'Описание';")->queryScalar();

            $DB->createCommand("COMMENT ON TABLE $tableScheme.$tableName IS '{$tableDescription}';")->queryScalar();
*/
            Alert::success("Успешное добавление таблицы");
            Scenario::run('refresh');
        }

        return $this->Template->renderView('/base-table/add', ['Model' => $Model]);
    }

    public function actionUpdateTable() {
        $DB = \Yii::$app->db;

        $tableScheme = "system";
        $tableName = "application";
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name IS 'Название';")->queryScalar();
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_lang IS 'Читаемое название';")->queryScalar();
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.name_short IS 'Короткое название';")->queryScalar();
        $DB->createCommand("COMMENT ON COLUMN $tableScheme.$tableName.description IS 'Описание';")->queryScalar();
        \system\components\Scenario::run('information', ['success']);
    }

}
