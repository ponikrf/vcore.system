<?php

namespace system\controllers;

/* basic system components */

use system\components\Notify;
use system\components\Scenario;
use system\components\Alert;
use system\components\UserControl;
use system\models\User;
use yii\data\ActiveDataProvider;
use \ext\web\Controller;

class UserController extends Controller {

    public function init() {
        parent::init();
        $this->Template->setTemplate('general');

        UserControl::setEntity('system', 'user');

        $this->Template->BreadCrumbs->add(['label' => 'Управление пользователями', 'url' => ['list']]);
        $this->Template->title = 'Пользователи системы';
    }

    public function actionLogin() {

        $this->Template->title = 'Login';

        $this->Template->setTemplate('login');

        $LoginForm = new \system\forms\Login();

        $referrer = $this->Request->get('refferer');

        if ($LoginForm->load($_POST) AND $LoginForm->validate()) {

            if ($this->User->Login($LoginForm)) {

                Notify::message('Login success', ['identify' => $LoginForm->username]);

                if ($referrer) {
                    Scenario::run('toRoute', [$referrer]);
                } else {
                    Scenario::run('toHome');
                }
            } else {
                Alert::error('Incorrect username or password');
                Notify::error('Incorrect username or password', ['identify' => $LoginForm->username]);
                Scenario::run('toLogin');
            }
        }
        return $this->Template->renderView('/user/login', array('model' => $LoginForm));
    }

    public function actionList() {

        UserControl::protectAction('show');

        $Query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $Query,
        ]);

        return $this->Template->renderView('/user/list', [
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionShow($id) {
        UserControl::protectAction('show');
        $Model = $this->getModel($id);

        $this->Template->BreadCrumbs->add(['label' => $Model->user_name,]);

        return $this->Template->renderView('/user/show', [
                    'Model' => $Model
        ]);
    }

    public function actionAdd() {

        UserControl::protectAction('add');

        $this->Template->BreadCrumbs->add(['label' => 'Добавить',]);

        $Model = new User;

        if (!$Model->user_locale) {
            $Model->user_locale = 'ru_RU';
        }

        if ($Model->load($this->Request->post()) AND $Model->validate()) {
            $Model->user_password = \vlibrary\util\Security::makeHash($Model->user_password);

            Notify::dbError($Model->insert(), $Model);
            $Model->user_password = '';

            Notify::info('Add new user', $Model->getAttributes(), Notify::INSERT);
            Scenario::run('toRoute', [$this->Route->make('list')]);
        }

        return $this->Template->renderView('/user/add', [
                    'Model' => $Model
        ]);
    }

    public function actionChangePassword($id) {

        UserControl::protectAction('change_password');
        

        $Model = $this->getModel($id);

        $this->Template->BreadCrumbs->add(['label' => $Model->user_name,'url'=>['show','id'=>$Model->id]]);
        $this->Template->BreadCrumbs->add(['label' => 'Установка (смена) пароля',]);
        
        
        if ($Model->load($this->Request->post()) AND $Model->validate()) {
            $Model->user_password = \vlibrary\util\Security::makeHash($Model->user_password);

            Notify::dbError($Model->save(), $Model);

            $Model->user_password = '';

            Notify::info('Update password for user', $Model->getAttributes(), Notify::UPDATE);
            Alert::info('Новый пароль успешно установлен');
            Scenario::run('toRoute', [$this->Route->make('list')]);
        } else {
            $Model->user_password = '';
        }

        return $this->Template->renderView('/user/change-password', [
                    'Model' => $Model
        ]);
    }

    public function actionEdit($id) {

        UserControl::protectAction('edit');

        $Model = $this->getModel($id);
        
        $this->Template->BreadCrumbs->add(['label' => $Model->user_name,'url'=>['show','id'=>$Model->id]]);
        $this->Template->BreadCrumbs->add(['label' => 'Редактирование']);
        
        if ($Model->load($this->Request->post()) AND $Model->validate()) {
            Notify::dbError($Model->save(), $Model);
            $Model->user_password = '';
            Notify::info('Update user', $Model->getAttributes(), Notify::UPDATE);
            Scenario::run('toRoute', [$this->Route->make('list')]);
        } else {
            $Model->user_password = '';
        }

        return $this->Template->renderView('/user/edit', [
                    'Model' => $Model
        ]);
    }

    public function actionClose($id) {

        UserControl::protectAction('close');

        $Model = $this->getModel($id);
    }

    public function getModel($id) {
        if (!$Model = User::find()->where(['id' => $id])->one()) {
            Scenario::run('information', ['objectNotFound']);
        }
        return $Model;
    }

    /**
     * Log out  
     * */
    public function actionLogout() {
        $this->User->logout();
        Notify::message('Logout user', ['id' => $this->User->getId()]);
        Scenario::run('toLogin');
    }

}
