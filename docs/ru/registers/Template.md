Templates
=========

Регистрирует шаблоны для компонента `Template`
 
Регистрация
-----------

Пример регистрации шаблонов:

    public function registerTemplates()
    {
		return [
		    'management' => $this->getBasePath() . '/templates/management',
		    'administration' => $this->getBasePath() . '/templates/administration',
		];
    }

Шаблоны регистрируются по полному пути до файла, без указания расширения. 

