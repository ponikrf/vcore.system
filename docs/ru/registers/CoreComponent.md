Core Component
==================
Регистрирует базовые компоненты системы, которые помещаются в системный DI `\Yii::$app`

Регистрация
-----------

Для того, чтобы зарегистрировать собственные компоненты системы, необходимо
реализовать метод для регистратора, например:


    public function registerCoreComponents()
    {
        return [
            'Template'  => ['class' =>  'system\components\Template'],
            'Notify'    => ['class' =>  'system\components\Notify'],
        ];
    }


После регистрации ваши компоненты будут доступны в DI `\Yii::$app->ComponentName`.

Вы можете наследовать `\ext\CoreComponent` для корректной работы 
вашего **core** компонента

