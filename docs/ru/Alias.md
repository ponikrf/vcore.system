Alias (namespace)
======

Зарезервированные имена и псевдонимы.
-------------------------------------
Каждому модулю в системе устанавливается личный псевдоним поэтому установка собственных псевдонимов должно сопровождаться определенными правилами.

Каждый модуль имеет уникальное имя, по этому имени создается псевдоним модуля, сделано это для того что бы исключить длинные строки в пространстве имен. 
Например, если мы используем модель собственного модуля, то мы можем просто указать на начало в виде имени модуля в вместо `app\modules\modulename\models\model` 
использовать `modulename\models\model` 
Отсюда следует, что имена модулей не могут содержать зарезервированные псевдонимы системы и самого фреймворка.

Вы можете добавлять свои псевдонимы напрямую в `\Yii::setAlias()` но вы должны учитывать условия указанные в документации системы и самого фреймворка.

Имена зарезервированные модулем:

* **@ext** - Расширения стандартных компонентов фреймворка
* **@vlibrary** - Расширения библиотеки vlibrary
* **@system** - Папка самого модуля