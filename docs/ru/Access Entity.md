Access Entity
=============
Управление доступом пользовательский функций
--------------------------------------------

**Access Entity** – сущности или функции, являющиеся абстрактным слоем между реальным функционалом, системы и системой предоставление доступа.

Данные сущности являются частью модуля, и содержатся непосредственно в нем. Модуль с помощью массива, сообщает системе, какие он имеет сущности и какие действия (action) имеют эти сущности.

Вот пример массива модуля user:

	return array( 
		'label' => 'Пользователи',

		'entity' => array(

			'management' => array(
				'actions' 	=> array('show','edit','delete'),
				'label' 	=> 'Управление',
			),

			'role' => array(
				'actions' 	=> array('show','edit','delete'),
				'label' 	=> 'Роли',
			),

			'permission' => array(
				'actions' 	=> array('show','edit'),
				'label' 	=> 'Разрешения',
			),
			
		),
		'actionsLabel' => array(
			'show' 		=> 'Просмотр',
			'edit' 		=> 'Редактирование',
			'delete' 	=> 'Удаление',
		),
	);


Как мы можем видеть, система сущностей сделана, так, что модуль может иметь не ограниченное количество сущностей, а также действий этих сущностей.

Формат передаваемого массива:

* **Label** - Описание набора сущностей данного модуля
* **Entity** - Cодержит массив самих сущностей, каждая сущность является массивом, в котором есть два параметра `actions` и `label`.	Параметр `actions` определяет набор действий. Параметр `label` это читаемое название сущности.
* **actionsLabel** - Читаемое описание действий сущностей