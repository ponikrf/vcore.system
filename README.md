VCore.System
=============

Базовый модуль, для фреймворка yii2, необходим для работы других модулей

Соглашение по использованию alias (namespace) см. `/docs/ru/Alias.md`

Что уже работает?
----------------

###Регистраторы 

Документацию на регистраторы можно найти в папке `/docs/ru/registers/`

Описание работы регистраторов в документе `/docs/ru/registers/Register.md`

 * Register - регистрирует регистраторы
 * CoreComponent - регистрирует базовые компоненты системы (доступны через `\Yii::$app->ComponentName`)
 * External - внешние, реализация слепых связей между модулями
 * Relation - регистрирует зависимости других модулей (в очень простом исполнении)
 * Scenarios - Регистратор для компонента `/system/components/Scenario`
 * Templates - Регистратор шаблонов для *core* компонента `/system/core/Template`
 * TextEntity - Регистрирует обработчики для компонента `/system/components/TextEntity`

### BaseObject

Описание базового объекта можно посмотреть в документе `docs/ru/BaseObject.md` 

### Доступ к компонентам через $this

В контроллерах и в представлениях вы можете использовать `$this->CoreComponent` вместо `\Yii->$app->CoreComponent`.
Для корректной работы необходимо наследовать `\ext\web\Controller.php`.

Установка
---------

Для установки потребуется подкорректировать стандартный `index.php`. 

Вы можете посмотреть `/index.php.Example` для примера. Правки нужны для создания
`alias` папок  `/system/ext/` и `system/vlibrary/` 

###Правки в файле конфигурации Yii2:

Вы можете посмотреть `/config.php.Example` для примера

Потребуется добавить модуль (стандартный способ Yii2)

    'modules' => [
        'system' => 'app\modules\system\Module',
    ],

В список компонентов добавить:

    'Register' => [
        'class' => 'app\modules\system\registers\Register',
    ],        
        
    'view' => ['class' => 'ext\base\View'],
        
    'errorHandler' => [
        'errorAction' => '/system/default/error',
    ],


Необходимо так же либо добавить либо дополнить параметр  `'bootstrap' => ['Register']`. 
Это необходимо для запуска регистраторов до запуска контроллера. 


### Конфигурация базы данных

Настройте соединение для базы данных Mysql, создайте базу и загрузите дамп `/vcore.sql`.