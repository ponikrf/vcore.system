<?php

namespace system\forms;

use Yii;

class BaseObject extends \yii\base\Model
{
    public $id;
    public $name;
    public $name_lang;
    public $name_short;
    public $description;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name_lang' => 'Читаемое название',
            'name_short' => 'Короткое название',
            'description' => 'Описание',
        ];
    }
}
