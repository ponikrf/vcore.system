<?php

namespace system\forms;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class Login extends Model
{
	public $username;
	public $password;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			// username and password are both required
			[['username','password'], 'required'],
			// password is validated by validatePassword()
		];
		
	}

	/**
	 * @return array the attribute labels
	 */
	public function attributeLabels()
	{
		return array(
			'username' => 'Имя пользователя',
			'password' => 'Пароль',
		);
	}
}
