<?php

namespace system\forms;

use Yii;

class BaseTable extends \yii\base\Model {

    public $scheme;
    public $table;
    public $description;

    public function attributeLabels() {
        return [
            'scheme' => 'Схема',
            'table' => 'Таблица',
            'description' => 'Описание',
        ];
    }

    public function rules() {
        return [
            // username and password are both required
            [['scheme', 'table'], 'required'],
            [['description', 'scheme', 'table'], 'safe'],
                // password is validated by validatePassword()
        ];
    }

}
