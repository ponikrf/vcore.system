<?php

use system\widgets\ButtonGroup;
use system\widgets\Button;

/*
 * Context type
 * 
 * default - grey
 * primary - blue
 * success - green
 * info - white blue
 * warning - yellow
 * danger - red
 * link - no formatted
 * 
 */

/*
 * Size type
 * 
 * no set for normal size
 * 
 * lg - large
 * sm - small
 * xs - extra small
 */

ButtonGroup::begin();
    echo Button::widget(['label' => 'test', 'size' => 'xs']);
    echo Button::widget(['label' => 'test', 'size' => 'xs']);
ButtonGroup::end();
