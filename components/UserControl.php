<?php

/**
 * Base array storage class
 * 
 * @author VTeam
 * @version 0.0.1a
 */

namespace system\components;

use \Exception;

/**
 * @link
 * @license
 * */
class UserControl {

    private static $_chackad;
    private static $_module;
    private static $_entity;
    private static $_user;
    private static $_guestAccess = [
        'system/user/login',
    ];

    public static function defaultProtection() {
        
	if (\Yii::$app->User->isGuest())
	{
	    foreach (self::$_guestAccess as $uri)
	    {
		if (\Yii::$app->Route->getRoute() == $uri)
		{
		    return TRUE;
		}
	    }
            Scenario::run('toLogin');
	}
    }

    public static function setEntity($module, $entity) {
        self::$_module = $module;
        self::$_entity = $entity;
    }

    public static function checkAction($action, $defaultProtection = FALSE) {
        if ((!self::$_module) OR ( !self::$_entity)) {
            throw new \Exception("Check action error - module and entity is not set!");
        }

        return self::check(self::$_module . '.' . self::$_entity . '.' . $action, $defaultProtection);
    }

    public static function protectAction($action) {
        self::checkAction($action, TRUE);
    }

    public static function check($userAccessString, $defaultProtection = FALSE) {

        if (!self::$_user) {
            self::$_user = \Yii::$app->User->getUser();
        }

        $accessArray = self::pharse($userAccessString);

        $accessArray['id_user_role'] = self::$_user->id_user_role;

        $Permission = \system\models\UserPermission::find()
                ->where($accessArray)
                ->one();

        if (!$Permission) {
            if ($defaultProtection) {
                self::runScenario();
            }
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public static function protect($userAccessString) {
        return self::check($userAccessString, TRUE);
    }

    public static function runScenario() {
        Scenario::run('information', ['accessDenied']);
    }

    public static function pharse($userAccessString) {

        $accessStringArray = explode('.', $userAccessString);

        if (count($accessStringArray) != 3) {
            throw new \Exception('Bad format access string - "' . $userAccessString . '"');
        }

        return [
            'module' => $accessStringArray[0],
            'entity' => $accessStringArray[1],
            'action' => $accessStringArray[2],
        ];
    }

}
