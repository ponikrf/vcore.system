<?php

/**
 * Base array storage class
 * 
 * @author VTeam
 * @version 0.0.1a
 */

namespace system\components;

use \Exception;

/**
 * @link
 * @license
 * */
class TextEntity {

    /**
     * @var \vlibrary\base\Helper 
     */
    private static $_operators;

    public static function init() {
        self::$_operators['system'] = function ($params) {
            return "[system function]";
        };
    }

    public static function addOperator($entity, $function) {
        self::$_operators[$entity] = $function;
    }

    public static function createEntity($entity, array $options = []) {
        $params = '';
        foreach ($options as $key => $value) {
            $params .= $key."='".$value."'";
        }
        return "[$entity $params]";
    }

    public static function replace($text) {

        if (!self::$_operators)
            self::init();

        $out = preg_replace_callback('/\[([^]]+)\]/', function ($match) {
            return TextEntity::runOperator($match);
        }, $text);

        return $out;
    }

    static protected function runOperator($match) {

        preg_match_all('/([a-zA-Z]+\=".*"|[^ ]+|\'.*\')/', $match[1], $out, PREG_PATTERN_ORDER);

        if (count($out) == 0)
            return '';

        $params = $out[0];
        $execFunc = '';
        $execParams = [];

        for ($i = 0; $i < count($params); $i++) {
            if ($i == 0) {
                $execFunc = $params[$i];
                continue;
            }
            $paramArray = preg_split('/\=/', $params[$i], 2);
            if (count($paramArray) > 1) {
                $execParams[$paramArray[0]] = str_replace(["'", '"'], '', $paramArray[1]);
            }
        }

        if (!array_key_exists($execFunc, self::$_operators)) {
            $execFunc = 'system';
        }

        $func = self::$_operators[$execFunc];

        return $func($execParams);
    }

}
