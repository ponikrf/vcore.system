<?php

/**
 * User control 
 * Base component for system user access control
 * 
 * 
 * @author VTeam
 * @version 0.0.1a
 *  
 * @see Off documentation (User)
 */

namespace system\components;

use Exception;
use ext\CoreComponent;
use vlibrary\base\Helper;

/**
 * @link
 * @license
 * */
class Scenario {

    /**
     * @var \system\components\FunctionStorage Scenarios storage
     */
    protected static $_scenarios;

    public static function addScenario($scenarioName, \Closure $function) {
        if (!self::$_scenarios instanceof \vlibrary\base\FunctionStorage) {
            self::$_scenarios = new \vlibrary\base\FunctionStorage();
        }
        return self::$_scenarios->addFunction($scenarioName, $function);
    }

    public static function run($scenarioName, array $scenarioConfig = []) {
        if (self::$_scenarios->hasFunction($scenarioName)) {
            return self::$_scenarios->execFunction($scenarioName,$scenarioConfig);
        } else {
            throw new Exception("Error - scenarion ($scenarioName) not found.", 1);
        }
    }
}

/* file  */
/* End of file */