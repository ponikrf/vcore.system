<?php

/**
 * @author VTeam
 */

namespace system\components;

use \Exception;

/**
 * @link
 * @license
 * */
class MoneyFormat {

    protected $decimalSeparator = '.';
    protected $thousandsSeparator = ',';
    protected $pennyLength = 2;

    public function getHelp() {
        $help = '';
        
        $help .= "Разделитель копеек - `".$this->decimalSeparator."` <br> \n\n ";
        $help .= "Разделитель тысячных - `".$this->thousandsSeparator."` <br> \n\n ";
        $help .= "Точность копеек - `".$this->pennyLength."` <br> \n\n ";
        return $help;
    }

    /**
     * Return formatted money string
     * 
     * @param int $pennyNumber Amount in penny
     * @param string $this->decimalSeparator Decimal separator 
     * @param string $this->thousandsSeparator Thousands separator
     * @param int $this->pennyLength Penny len
     * @return mixed string or false if not formatted
     * */
    public function renderMoney($pennyNumber) {
        return number_format($pennyNumber / 100, $this->pennyLength, $this->decimalSeparator, $this->thousandsSeparator);
    }

    /**
     * Converted Amount to penny
     * 
     * @param string $stringMoney amount string
     * @param string $this->decimalSeparator Decimal separator 
     * @param string $this->thousandsSeparator Thousands separator
     * @param int $this->pennyLength Penny len
     * @return mixed False or penny 
     * */
    public function renderPenny($stringMoney) {
        
        $penny = FALSE;
        
        if (strpos($stringMoney, $this->decimalSeparator) !== FALSE) {
            $penny_array = explode($this->decimalSeparator, $stringMoney);
            /* Valid penny format? */
            if ((strlen($penny_array[1]) !== $this->pennyLength AND $this->pennyLength !== 0) OR is_numeric($penny_array[1]) == false)
                return FALSE;
            else {
                $stringMoney = $penny_array[0];
                $penny = $penny_array[1];
            }
        }

        /* check thousands separator in money */
        if (strpos($stringMoney, $this->thousandsSeparator) !== FALSE AND $this->thousandsSeparator !== FALSE) {
            $money_array = explode($this->thousandsSeparator, $stringMoney);
            $stringMoney = '';
            $idx = 0;
            foreach ($money_array as $value) {
                $stringMoney = $stringMoney . $value;
                if ((strlen($value) < 3 AND $idx > 0) OR ( strlen($value) > 3) OR ( is_numeric($value) === FALSE))
                    return FALSE;
                $idx++;
            }
        }

        if ($penny === FALSE AND is_numeric($stringMoney) !== FALSE) {
            $stringMoney = $this->pennyLength > 0 ? $stringMoney * pow(10, $this->pennyLength) : $stringMoney;
        } else {
            $stringMoney = $stringMoney . $penny;
        }
        /* if result is numeric */
        if (is_numeric($stringMoney))
            return (int) $stringMoney;
        else
            return FALSE;
    }

}
