<?php

/**
 * Default controller
 * ==========================
 * 
 * general function
 * -----------------
 * 
 * * index
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace system\components;

use yii\base\Object;
use catalog\models\Catalog;

/**
 * @link
 * @license
 * */
class BaseObject extends Object
{
    /* field array */
    public static function getArray($models,$default=[],$key='id',$value='name_lang')
    {
	if (!$models){
	    return $default;
	}

	$returnArray = $default;

	foreach ($models as $model)
	{
	    $returnArray[$model->$key] = $model->$value;
	}
	
	return $returnArray;
    }

}
