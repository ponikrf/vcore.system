<?php
/**
 * @see components/Alert
 * 
 * @author VTeam
 * @version 0.0.1a
 *  
 */

namespace system\components;

/**
 * @link
 * @license
 * */

class Alert extends \yii\base\Object
{
    const ALERT_ERROR = 'danger';
    const ALERT_WARNING = 'warning';
    const ALERT_MESSAGE = 'info';
    const ALERT_INFO = 'info';
    const ALERT_SUCCESS = 'success';

    /**
     * Save to flash session notify for a public on next page
     * 
     * @param 	string		notify
     * @param	string		notify header 
     * */
    public static function alert($notify, $level = '', $notifyHeader = NULL)
    {
        \Yii::$app->getSession()->setFlash('publicAlert', array('notify' => $notify, 'status' => $level, 'notifyHeader' => $notifyHeader));
    }

    public static function error($notify, $header = FALSE)
    {
        self::alert($notify, static::ALERT_ERROR, $header);
    }

    public static function warning($notify, $header = FALSE)
    {
        self::alert($notify, static::ALERT_WARNING, $header);
    }

    public static function message($notify, $header = FALSE)
    {
        self::alert($notify, static::ALERT_MESSAGE, $header);
    }

    public static function info($notify, $header = FALSE)
    {
        self::alert($notify, static::ALERT_INFO, $header);
    }

    public static function success($notify, $header = FALSE)
    {
        self::alert($notify, static::ALERT_SUCCESS, $header);
    }
}

/* file  */
/* End of file */