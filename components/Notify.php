<?php

/**
 * Base notify component
 * 
 * 
 * @author VTeam
 * @version 0.0.1a
 *  
 * @see system/notify.ph
 */

namespace system\components;

use \ext\CoreComponent;
use \Exception;

/**
 * @link
 * @license
 * */
class Notify extends CoreComponent {

    /**
     * @var Level system notify const
     * */
    const LVL_ERROR = 1;
    const LVL_WARNING = 2;
    const LVL_MESSAGE = 3;
    const LVL_INFO = 4;
    const LVL_SYSTEM = 5;

    const INSERT = 1;
    const UPDATE = 2;
    const DELETE = 3;
    const INFO = 4;

    /**
     * @var private session var
     * */
    private static $_session = FALSE;

    private static function _generateSession() {
        if (!self::$_session) {
            self::$_session = md5(uniqid() . 'notifySession');
        }
    }

    /**
     * Save error message
     * 
     * @param	string	error message	
     * @param	string	external data
     * */
    public static function error($notify, $data = array(), $action = 4) {
        self::saveMessage($notify, static::LVL_ERROR, $data,$action);
    }

    /**
     * Save warning message
     * 
     * @param	string	warning message	
     * @param	string	external data
     * */
    public static function warning($notify, $data = array(), $action = 4) {
        self::saveMessage($notify, static::LVL_WARNING, $data,$action);
    }

    /**
     * Save message
     * 
     * @param	string	message	
     * @param	string	external data
     * */
    public static function message($notify, $data = array(), $action = 4) {
        self::saveMessage($notify, static::LVL_MESSAGE, $data,$action);
    }

    /**
     * Save info message
     * 
     * @param	string	info message	
     * @param	string	external data
     * */
    public static function info($notify, $data = array(), $action = 4) {
        self::saveMessage($notify, static::LVL_INFO, $data,$action);
    }

    /**
     * Save system message
     * 
     * @param	string	system message	
     * @param	string	external data
     * */
    public static function system($notify, $data = array(), $action = 4) {
        self::saveMessage($notify, static::LVL_SYSTEM, $data,$action);
    }

    /**
     * Save system message
     * 
     * @param	string	system message	
     * @param	string	external data
     * */
    public static function dbError($result = false,$model) {
        if (!$result){
            self::saveMessage('Database error', static::LVL_ERROR, $model->errors,static::INFO);
            Alert::error("Database error [".print_r($Model->errors,TRUE)."]");
        }
    }

        
    
    
    /**
     * Save message to storage
     * 
     * @param	string		Message text
     * @param	int             system level 
     * @param	string		External data
     * */
    private static function saveMessage($message, $lvl = 5, $data, $action) {
        if (!is_array($data)) {
            throw new Exception("There must be an array", 1);
        }
        self::_generateSession();
        
        $notify = new \system\models\Notify();
        $notify->id_application = 1;
        $notify->id_notify_level = $lvl;
        $notify->id_notify_action = $action;
        $notify->id_user = \Yii::$app->User->getId();
        $notify->notify_text = $message;
        $notify->notify_data = json_encode($data);
        $notify->insert();
    }

}

/* file  */
/* End of file */