<?php

/**
 * Helper for work on datetime of a system
 * 
 * Helper have a base function for work on a DB datetime and system datetime
 * 
 * @author VTeam
 * @version 0.0.1a
 * 
 * Other
 * @see please dont set standart config variable 'modules' if you do not want a crash this system
 */

namespace system\helpers;

/**
 * @link
 * @license
 * */
class DateTimeHelper extends \yii\base\Object
{
    /*     * ***************** System format ***************************** */

    /** @var system date format */
    const DATE_FORMAT = 'Y-m-d';

    /** @var system date time format */
    const DATETIME_FORMAT = 'Y-m-d H:i:s';

    /** @var system time format */
    const TIME_FORMAT = 'H:i:s';

    /*     * **************** For file names ***************************** */

    /** @var date format for file names */
    const FILE_DATE_FORMAT = 'His';

    /** @var date format for file names */
    const FILE_DATETIME_FORMAT = 'His H.i.s';

    /*     * ****************  Database  formats  ************************ */

    /** @var Database date format */
    const DB_DATE_FORMAT = 'Y-m-d';

    /** @var Database date time format */
    const DB_DATETIME_FORMAT = 'Y-m-d H:i:s';


    /*
     *  Data status on date period
     * */

    /** Active record data */
    const DATA_ACTIVE = 2;

    /** Inactive record data */
    const DATA_INACTIVE = 3;

    /** close data */
    const DATA_CLOSE = 4;

    /**
     * Return a system date now
     * 
     * @return string system date
     */
    static function date()
    {
	return date(static::DATE_FORMAT);
    }

    /**
     * Return a system datetime  now
     * 
     * @return string system date time now
     */
    static function dateTime()
    {
	return date(static::DATETIME_FORMAT);
    }

    /**
     * Return  database date format
     * 
     * @return string now date for database
     */
    static function DBDate()
    {
	return date(static::DB_DATE_FORMAT);
    }

    /**
     * Helper function for a system database datetime format
     * 
     * @return string now datetime for database
     */
    static function DBDateTime()
    {
	return date(static::DB_DATETIME_FORMAT);
    }

    /**
     * Return a date stamp for file name
     * 
     * @return  string date stamp for file name
     */
    static function fileDate()
    {
	return date(static::FILE_DATE_FORMAT);
    }

    /**
     * Return a date time stamp for file name
     * 
     * @return  string date time  stamp for file name
     */
    static function fileDateTime()
    {
	return date(static::FILE_DATETIME_FORMAT);
    }

    /**
     * Convert to unix time format
     * from strtotime support format
     * 
     * @param string $date strtotime support format date
     * @return mixed strtotime result 
     */
    static function normalizeDate($date)
    {
	return strtotime($date);
    }

    /**
     * Render strtotime support format date to national date
     * 
     * @param string $date strtotime support fromat date
     * @return string	 full date
     * @see self::toDate
     */
    static function renderDate($date)
    {
	if (!$date)
	{
	    return NULL;
	}
	return self::toDate(self::normalizeDate($date));
    }

    /**
     * Render strtotime support format date to national datetime
     * 
     * @param string $date strtotime support fromat datetime
     * @return string	 full datetime
     * @see self::toDate
     */
    static function renderDatetime($datetime)
    {
	if (!$datetime)
	{
	    return NULL;
	}
	return self::toDatetime(self::normalizeDate($datetime));
    }

    /**
     * Convert unix time to national date
     * 
     * @param int $unixTime time to unix format 
     * @return string full date
     */
    static function toDate($unixTime)
    {
	$day = date('d', $unixTime);
	$month = date('m', $unixTime);
	$year = date('Y', $unixTime);

	return $day . '&nbsp;' . self::toMoth($month) . '&nbsp;' . $year;
    }

    /**
     * Convert unix time to national datetime
     * 
     * @param int $unixTime time to unix format 
     * @return string full datetime
     */
    static function toDatetime($unixTime)
    {
	$day = date('d', $unixTime);
	$month = date('m', $unixTime);
	$year = date('Y', $unixTime);

	$time = date(self::TIME_FORMAT, $unixTime);

	return $day . '&nbsp;' . self::toMoth($month) . '&nbsp;' . $year . '&nbsp;' . $time;
    }

    /**
     * Convert month number to national format
     * 
     * @param int $month month number
     * @return string month string
     */
    static function toMoth($month)
    {
	$montharr = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
	if (array_key_exists($month - 1, $montharr))
	{
	    return $montharr[$month - 1];
	}
	else
	{
	    throw new Exception("Month [$month] not found");
	}
    }

    /**
     * Return a count invervals between $startDatetime and $endDatetime
     * 
     * @param string $interval Interval ['SECOND','MIN','HOUR','DAY']
     * @param int $startDatetime Normalize start datetime 
     * @param int $endDatetime Normalize end datetime 
     * @return int interval count
     */
    public static function datetimeDiff($interval, $startDatetime, $endDatetime)
    {
	$countSecond = (int) $endDatetime - $startDatetime;

	if ($countSecond < 0)
	{
	    return 0;
	}
	switch ($interval)
	{
	    case 'SECOND' :
		return self::secondDiff($countSecond);
	    case 'MIN':
		return self::minDiff($countSecond);
	    case 'HOUR':
		return self::hourDiff($countSecond);
	    case 'DAY':
		return self::dayDiff($countSecond);
	}
	return 0;
    }

    /**
     * Return a count second in $countSecond
     * 
     * @param int $countSecond Secound count 
     * @return count second
     */
    public static function secondDiff($countSecond = 0)
    {
	return (int) $countSecond;
    }

    /**
     * Return a count min in $countSecond
     * 
     * @param int $countSecond Secound count 
     * @return count min
     */
    public static function minDiff($countSecond = 0)
    {
	return (int) $countSecond / 60;
    }

    /**
     * Return a count hour in $countSecond
     * 
     * @param int $countSecond Secound count 
     * @return count min
     */
    public static function hourDiff($countSecond = 0)
    {
	return (int) $countSecond / 3600;
    }

    /**
     * Return a count day in $countSecond
     * 
     * @param int $countSecond Secound count 
     * @return count day
     */
    public static function dayDiff($countSecond = 0)
    {
	return (int) $countSecond / 86400;
    }

    /**
     * Return date time active status
     * 
     * @param string $dateStart start date
     * @param string $dateEnd end date
     * @param string $dateTime now date time
     * @return bool true if active and false if inactive
     * */
    public static function datetimeStatus($dateStart = FALSE, $dateEnd = FALSE, $dateTime = false)
    {
	if ($dateStart === FALSE)
	{
	    return FALSE;
	}
	if (!$dateTime)
	{
	    $dateTime = self::DBDateTime();
	}

	$startTime = strtotime($dateStart);
	$endTime = strtotime($dateEnd);
	$deathTime = NULL;
	$nowTime = strtotime($dateTime);

	if ($endTime > $deathTime AND $nowTime > $endTime AND $nowTime > $startTime)
	{
	    return FALSE;
	}
	if ($startTime < $nowTime AND ($deathTime == $endTime OR $nowTime < $endTime))
	{
	    return TRUE;
	}
    }

}
