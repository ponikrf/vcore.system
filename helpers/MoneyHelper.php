<?php

namespace system\helpers;

class MoneyHelper {

    public static $formatters = [];

    public static function addConfig($format, \system\components\MoneyFormat $formatter) {
        self::$formatters[$format]=$formatter;
    }

    public static function toMoney($penny, $format = 'default') {
        return self::$formatters[$format]->renderMoney($penny);
    }

    public static function toPenny($penny, $format = 'default') {
        return self::$formatters[$format]->renderPenny($penny);
    }

}
