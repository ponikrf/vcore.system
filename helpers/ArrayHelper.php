<?php

/**
 * Array helper
 * @author VTeam
 * @version 0.0.1a
 * 
 * Other
 * @see please dont set standart config variable 'modules' if you do not want a crash this system
 */

namespace system\helpers;

/**
 * @link
 * @license
 * */
class ArrayHelper
{

    /**
     * Array to string
     * 
     * @param array $array Convert array to string
     */
    public static function toString($array, $separator = ',', $subSeparator = ',')
    {
        if (!$array){
            return NULL;
        }
	$returnString = NULL;
	$index = 0;
	foreach ($array as $value)
	{
	    if (is_array($value))
	    {
		$value = static::toString($value,$subSeparator,$subSeparator);
	    }

	    if ($index > 0)
	    {
		$value = $separator . $value;
	    }

	    $returnString .= $value;
	    $index++;
	}
	return $returnString;
    }

    public static function arrayToCsv($array = array())
    {
	$retrunString = '';
	foreach ($array as $key => $value)
	{
	    if ($key != 0)
	    {
		$retrunString .= ';' . $value;
	    }
	    else
	    {
		$retrunString .= $value;
	    }
	}
	return $retrunString . "\n";
    }    
    
}
