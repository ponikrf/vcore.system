<?php

/**
 * File helper
 * @author VTeam
 * @version 0.0.1a
 * 
 * Other
 * @see please dont set standart config variable 'modules' if you do not want a crash this system
 */

namespace system\helpers;

/**
 * @link
 * @license
 * */
class FileHelper {

    /**
     * scan directory and return array 
     * ('files' => $files, 'dirs' => $dirs)
     * 
     * @access 		public
     * @param 		string 		directory path
     * @return 		array 		see header 	
     * */
    public static function scanDir($dirPath) {
        $path = realpath($dirPath);
        $path = $path;
        $files = array();
        $filesInfo = array();
        $directory = array();

        if ($fp = @opendir($path)) {
            while (FALSE !== ($file = readdir($fp))) {

                if (is_dir($path . DIRECTORY_SEPARATOR . $file) && strncmp($file, '.', 1) !== 0) {
                    $directory[] = $file;
                } elseif (strncmp($file, '.', 1) !== 0) {
                    $files[] = $file;
                    $filesInfo[$file] = $this->getFileInfo($path . DIRECTORY_SEPARATOR . $file);
                }
            }
            return array('files' => $files, 'dirs' => $directory, 'filesinfo' => $filesInfo);
        } else {
            return FALSE;
        }
    }

    /**
     * Check is the file is a file relative to the base path
     * return true if this is file is a not directory or empty 
     * 
     * @access public 
     * @param 	string	file path
     * @return 	bool	true or false
     * */
    public static function isFile($filePath) {
        if (!is_dir($filePath) AND file_exists($filePath)) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Check is the dir is a dir relative to the base path
     * return true if this is dir is a not file or empty 
     * 
     * @access public 
     * @param 	string	file path
     * @return 	bool	true or false
     * */
    public static function isDir($dirPath) {
        if (is_dir($dirPath)) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Convert byte to round byte system
     * 
     * @access public
     * @param 	string 	bytes
     * @param   int		precision
     * @return  string		true or false
     * */
    public static function roundFileSize($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);
        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
     * Return extension of a file name
     * 
     * @access public
     * @param 	string 	file name
     * @return  bool 	true or false
     * */
    public static function fileExtension($fileName) {
        $_filename_array = explode('.', $filename);
        if (count($_filename_array) > 1)
            return end($_filename_array);
        else
            return '';
    }

    /**
     * Checks Correctly file name
     * 
     * @access public
     * @param 	string 	file name
     * @return  bool 	true or false
     * */
    public static function chackFileName($fileName) {
        if (preg_match("/(^[a-zA-Z0-9]+([a-zA-Z\_0-9\.-]*))$/", $fileName) AND strlen($fileName) < 255)
            return TRUE;
        else
            return FALSE;
    }

}
