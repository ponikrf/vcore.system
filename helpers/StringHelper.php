<?php

/**
 * Array helper
 * @author VTeam
 * @version 0.0.1a
 * 
 * Other
 * @see please dont set standart config variable 'modules' if you do not want a crash this system
 */

namespace system\helpers;

/**
 * @link
 * @license
 * */
class StringHelper {

    public static $charset = 'UTF-8';

    public static function ln($text) {
        echo $text."<br>\n";
    }

    public static function toFullName($text) {
        if ($text == FALSE)
            return '';

        return mb_convert_case($text, MB_CASE_TITLE, self::$charset);
    }

    public static function firstChar($string) {
        return mb_substr($string, 0, 1, self::$charset);
    }

    public static function cleanInput($input) {

        $search = array(
            '@<script[^>]*?>.*?</script>@si', // Strip out javascript
            '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
            '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
            '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
        );

        $output = preg_replace($search, '', $input);
        return $output;
    }

}
