<?php
/**
 * Registration for template system
 * 
 * Every module register a self templates
 * 
 * @author VTeam
 * @version 0.0.1a
 * 
 */
 
namespace system\registers;
use \ext\base\Register;

/**
 * @link
 * @license
 * */ 

class Templates extends Register
{
	/**
	 * Register function
	 * 
	 * @access 	public
	 * @param 	array 	register array from module 
	 * @param	string 	registration module name 
	 * @return 	null
	 * */
	public function register(array $registerArray,$moduleName)
	{
		if (empty($registerArray)) 
		{
			return NULL;
		}
		
		foreach ($registerArray as $template => $path)
		{
			$this->set($template,$path);
		}
	}
	
	/**
	 * Return template array
	 * 
	 * @param	string		template name
	 * @return	array		template array
	 * */
	public function getTemplate($templateName)
	{
		if (!$this->has($templateName))
		{
			throw new \Exception("Error of local storage - template [$templateName] not found");
		}
		return include(\Yii::getAlias($this->get($templateName)).'.php');	
	}
}
/* file  */
/* End of file */