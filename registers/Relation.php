<?php

namespace system\registers;

use ext\base\Register;
use Exception;

class Relation extends Register {

    protected $modulesArray;

    public function init() {
        parent::init();
        $this->modulesArray = \Yii::$app->getModules();
    }

    public function register(array $registerArray, $moduleName) {
        $ErrorRelation = [];
        foreach ($registerArray as $relation) {
            if (!array_key_exists($relation, $this->modulesArray)){
                $ErrorRelation[$moduleName][]=$relation;
            }
        }
        
        if (!empty($ErrorRelation)){
            echo "<p>Error relation module!</p>";
            echo "<p>Module => list a need module</p>";
            echo "<pre>";
                print_r($ErrorRelation);
            echo "</pre>";
            die();
        }
    }

}
