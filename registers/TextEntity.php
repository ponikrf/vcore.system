<?php

namespace system\registers;

use ext\base\Register;
use Exception;


class TextEntity extends Register {
    public function register(array $registerArray, $moduleName) {
        foreach ($registerArray as $object => $function) {
            if (is_callable($function)) {
                $this->set($object, $function);
                \system\components\TextEntity::addOperator($object, $function);
            }
        }
    }

}

/* file  */
/* End of file */