<?php
/**
 * Registers for a base components
 * 
 * 
 * 
 * 
 * @author VTeam
 * @category Register
 * @package app.registers
 * @version 0.0.1
 */
 
namespace system\registers;

use ext\base\Register;
use Exception;

class CoreComponents extends Register 
{
	/**
	 * Register function
	 * 
	 * @access 	public
	 * @param 	array 	register array from module 
	 * @param	string 	registration module name 
	 * @return 	null
	 * */
	public function register(array $registerArray,$moduleName)
	{
		foreach ($registerArray as $id => $component) 
		{
			$Object = \Yii::createObject($component);
			$this->set($id, $Object);
			\Yii::$app -> set($id,$Object);
		}
		
	}
}
/* file  */
/* End of file */