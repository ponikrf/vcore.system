<?php
/**
 * Implements its own module loading
 * 
 * Register external for module
 * 
 * register array(
 * 		'implement' 	=> 'module for'
 * 		'task'			=> 'task ID'
 * 		'class'			=> 'classname';
 * );
 * 
 * @author VTeam
 * @category Components
 * @package app.core.loader
 * @subpackage core
 * @version 0.0.1
 */
namespace system\registers;

use \ext\base\Register;

use Exception;

class Externals extends Register
{
	/** @var Local storage externals registration */
	private $_externals;

	/** @var directory name for a place external controllers */
	public $externalDirectory = 'externals';
	
	/** @var */
	public $externalsCache;
	
	/**
	 * Init at autoload
	 * 
	 * @access public
	 * @return void
	 * */
	public function init()
	{
		$this -> _externals = array(); 
	}
	
	/**
	 * Register external for module
	 * 
	 * register array(
	 * 		'implement' 	=> 'module for'
	 * 		'task'			=> 'task ID'
	 * 		'class'			=> 'classname';
	 * );
	 * 
     * @access  public
     * @param   array       register array
     * @param   string      module name
     * @return  NULL
	 * */	
	public function register(array $registerArray, $moduleName)
	{
		if (empty($registerArray))
		{
			return NULL;
		}
		
		if (array_key_exists('implement', $registerArray) AND array_key_exists('task', $registerArray)  AND array_key_exists('class', $registerArray))
		{
			$this -> _externals[$registerArray['implement']][$registerArray['task']][$moduleName] = $registerArray['class'];
		}
		else
		{
			if (!is_array($registerArray))
			{
				throw new Exception("This is a bad format of the array for a external", 1);
			}

			foreach ($registerArray as $key => $value)
			{
						$this -> register($value, $moduleName);
			}
		}
		
	}
	
	
	/**
	 * load external classes and return the moduleName => controller array 
     * 
     * @access public
     * @param  string   implement 
     * @param  string   task
     * @return array    class array or empty array
	 * */
	public function createAll($implement,$task)
	{
	    if (!$this -> has($implement, $task)){ return []; }
		
		if (isset($this->externalsCache[$implement][$task]))
		{
			return $this->externalsCache[$implement][$task];
		}
		
		/**
         * If there is something there %-)
         * fill the array
         * */
     	foreach ($this -> _externals[$implement][$task] as $ModuleName => $className) 
		{
			 $this->externalsCache[$implement][$task][$ModuleName] = $this -> createOne($ModuleName, $className);
		}
        return $this->externalsCache[$implement][$task];
	}
	
	/**
	 * Create external class
	 * 
	 * @access 	public
	 * @param  	string	module name (id)
	 * @param 	string	class name (a external class controller)
	 * @return 	mixed 	object or null if class not found
	 * */
	public function createOne($moduleName,$className)
	{
		$module = \Yii::$app -> getModule($moduleName);
		if (preg_match('/^[A-Za-z0-9\\-_]+$/', $className))
		{
			$externalPath = $module -> getBasePath() . DIRECTORY_SEPARATOR . $this -> externalDirectory . DIRECTORY_SEPARATOR;
		    $id = strtolower($className);
			$className = $className;
			
			$externalFile = $externalPath.$className.'.php';
			$className = ltrim('' . $moduleName . '\\' . $this -> externalDirectory . '\\' . $className, '\\');
			return new $className($id,$module);
		}
		else
		{
			throw new Exception("Bad external class name ({$className})");
		}
	}

    /**
     * Chack a have this external class
     * 
     * @access  public 
     * @param   string      implement class
     * @param   string      task 
     * @param   string      module name for a check have this task and implement
     * @return  bool        true or false
     * */
    public function has($implement,$task,$moduleName = false)
    {
        if (!array_key_exists($implement,$this -> _externals))
            return FALSE;

		if (!array_key_exists($task, $this -> _externals[$implement]))
		{
			return FALSE;
		}

		if ($moduleName == false)
		{
			return TRUE;
		}
		
		return array_key_exists($moduleName, $this -> _externals[$implement][$task]);
	}


    /**
     * Return a external list array on this task
     * 
     * @access  public 
     * @param   string      implement class
     * @param   string      task 
     * @param   string      module name for a check have this task and implement
     * @return  bool        true or false
     * */
	public function getTask($implement,$task)
	{
		if ($this -> has($implement, $task))
		{
			return $this -> _externals[$implement][$task];
		}
	}
	
	
	
    /**
     * Give a external array
     * 
     * @access  public 
     * @param   string      implement class
     * @param   string      task 
     * @param   string      module name for a check have this task and implement
     * @return  bool        true or false
     * */
    public function getArray()
    {
        return $this -> _externals; 
    }
}
/* file  */
/* End of file */