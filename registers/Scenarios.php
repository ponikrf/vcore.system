<?php

namespace system\registers;

use ext\base\Register;
use Exception;


class Scenarios extends Register {
    public function register(array $registerArray, $moduleName) {
        foreach ($registerArray as $object => $function) {
            if (is_callable($function)) {
                $this->set($object, $function);
                \system\components\Scenario::addScenario($object, $function);
            }
        }
    }

}

/* file  */
/* End of file */