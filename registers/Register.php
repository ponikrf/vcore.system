<?php
/**
 * Implements its own module loading
 * 
 * 
 * @author VTeam
 * @category Components
 * @package app.core.loader
 * @subpackage core
 * @version 0.0.1
 */
 
namespace app\modules\system\registers;

use ext\CoreComponent;

use Exception;

class Register extends CoreComponent
{
	
	/** @var Register the config components list  */
	public $loadRegisters;
	
	/** @var Register components list */
	private $_registers = array();
	
	/** @var prefix  for a registration method a module   */
        private $_methodPrefix = 'register';
        

	/**
	 * Init at autoload
	 * 
	 * @access public
	 * @return void
	 * */
	public function init()
	{
		$modules = \Yii::$app -> getModules();
		$modulesObject = [];
		foreach ($modules as $moduleName => $module)
		{
			$modulesObject[$moduleName] = \Yii::$app -> getModule($moduleName);	
			
			/**
			 * Setting alias name for module. You can use alias for local resourse
			 * simple moduleName\model\modelName for a place app\modules\moduleName\models\modelName
			 * @see YiiBase::setAlias();
			 * */
			if ($modulesObject[$moduleName] instanceof \ext\base\Module){
			    \Yii::setAlias('@'.$moduleName,$modulesObject[$moduleName] -> getBasePath());
			}
			if (method_exists($modulesObject[$moduleName],'registers'))
			{
				$thisMethod = 'registers';
				$this -> register($modulesObject[$moduleName] -> $thisMethod(), $moduleName);
			}
		}

		foreach ($modulesObject as $moduleName => $module) 
		{
			foreach ($this -> _registers as $className => $component) 
			{
				if (!method_exists($module, $this -> _methodPrefix.$className))	continue;
				
				$thisMethod = $this -> _methodPrefix.$className;
				$component -> register($module->$thisMethod(),$moduleName);
			}
		}
                
                \system\components\UserControl::defaultProtection();
	}
	
	/**
	 * Function for register registers
	 * 
	 * @param	array 		register array
	 * @param	string		module name
	 * @return 	null
	 * */
	private function register(array $registerArray,$moduleName)
	{
		foreach ($registerArray as $componentName => $componentClass)
		{
                        $object = \Yii::createObject($componentClass);
			$this -> _registers[end(explode('\\',get_class($object)))] = $object;
		}
	}
		
	/**
	 * getter for getting register
	 * */
	public function __get($getName)
	{
		return $this -> get($getName);
	}

	/**
	 * return a module object or false if module not found
	 * 
	 * @access  public 
	 * @param	string	module name
	 * @return  array 	modules object array[moduleName] = object  
	 * */
	public function get($registerName)
	{
		if ($this -> has($registerName))
			return $this -> _registers[$registerName];
		else
			throw new Exception("Register ($registerName) not found");
	}

	/**
	 * return loaded module status
	 * @access 	public
	 * @param 	
	 * @return 	bool	true or false if module not loaded
	 * */
	public function has($registerName)
	{
		return array_key_exists($registerName, $this -> _registers);
	}

	/**
	 * return a modules list
	 * 
	 * @access	public 
	 * @return  array 		array[]=$moduleName
	 * */
	public function getList()
	{
		$returnArray = array();
		
		foreach ($this -> _registers as $key => $value) 
			$returnArray[] = $key;
		
		return $returnArray;
	}
}
/* file  */
/* End of file */