<?php

/**
 * Universal base template class
 * 
 * 
 * @author VTeam
 * @version 0.0.1a
 *  
 * @see Off documentation (User)
 */

namespace system\core;

use \ext\CoreComponent;
use Exception;

/**
 * @link
 * @license
 * */
class Template extends CoreComponent {

    public $templateDir;
    public $activeTemplate = FALSE;
    public $_template;
    private $_viewParams = [];

    /**
     * Construct
     */
    public function __construct() {
        $this->_template = array();
        $this->templateDir = __DIR__ . '/../templates/';
    }

    /**
     * Return active template name 
     * 
     * @return string active template name or false if not set
     */
    public function activeTemplate() {
        return $this->activeTemplate;
    }

    public function setViewParams(array $params = []) {
        $this->_viewParams = $params;
    }

    public function addViewParams(array $params) {
        $this->_viewParams = array_merge($this->_viewParams, $params);
    }

    /**
     * Set a template array
     * 
     * @access 	public 
     * @param	array 	template array
     * @return 	null
     * */
    public function setArray($templateArray) {
        $this->_template = $templateArray;
    }

    /**
     * Set a file storge array
     * 
     * @access 	public 
     * @param 	string	string	 file name for a system/template/$filename 
     * @return 	null
     * */
    public function setTemplate($templateName) {
        $this->_template = \Yii::$app->Register->Templates->getTemplate($templateName);
        $this->activeTemplate = $templateName;
    }

    /**
     * Bad function - not for use
     * 
     * @access 	public 
     * @param	string		NOT USE THIS FUNCTION!!!
     * @return 	null
     * */
    public function has($key) {
        return array_key_exists($key, $this->_template);
    }

    /**
     * Getter for a template array objects
     * 
     * @access 	public
     * @param	string	key
     * @return 	null 	
     * */
    public function __get($key) {
        if (array_key_exists($key, $this->_template)) {
            return $this->_template[$key];
        } else {
            throw new Exception("Error, this key ($key) not found in the template array", 1);
        }
    }

    /**
     * Getter for a template array objects
     * 
     * @access 	public
     * @param	string	key
     * @return 	null 	
     * */
    public function get($key) {
        if (array_key_exists($key, $this->_template)) {
            return $this->_template[$key];
        } else {
            throw new Exception("Error, this key ($key) not found in the template array", 1);
        }
    }

    /**
     * Setter for a template array object
     * 
     * @access 	public
     * @param string key
     * @param string $value 
     * @return 	null 	
     * */
    public function set($key, $value = FALSE) {
        $this->_template[$key] = $value;
    }

    /**
     * Render a html to layout if it found 
     * 
     * @param	string		content of document
     * @param	array		params to layout
     * */
    public function renderHtml($content, $params) {
        if ($this->has('layout')) {
            $params['content'] = $content;
            return \Yii::$app->getView()->render($this->get('layout'), $params);
        }
        return $content;
    }

    /**
     * Render MarkDown file 
     * 
     * @param	string		alias to file
     * @param	array		params (use for layout)
     * */
    public function renderMD($alias, $params = []) {
        if ($this->has('layout')) {
            $params['content'] = \yii\helpers\Markdown::process(file_get_contents(\Yii::getAlias($alias)));
            return \Yii::$app->getView()->render($this->get('layout'), $params);
        }
        return $params['content'] = \yii\helpers\Markdown::process(file_get_contents(\Yii::getAlias($alias)));
        ;
    }

    /**
     * Render a view file standart view render
     * 
     * @param	string		path to view
     * @param	array		params array
     * @return	string		render result
     * */
    public function renderView($view, $params) {
        $content = \Yii::$app->getView()->render($view, array_merge($this->_viewParams,$params));
        return $this->renderHtml($content, $params);
    }

    /**
     * Render a view file standart view render
     * 
     * @param	string		path to view
     * @param	array		params array
     * @return	string		render result
     * */
    public function renderPartial($view, $params) {
        return \Yii::$app->getView()->render($view, array_merge($this->_viewParams,$params));
    }

    /**
     * Setter for a set template array value
     * 
     * @access 	public
     * @param	string	key
     * @param	mixed	value 
     * @return 	null 	
     * */
    public function __set($key, $value) {
        $this->_template[$key] = $value;
    }

}

/* file  */
/* End of file */