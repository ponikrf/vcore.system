<?php

/**
 * Authentication user component
 * 
 * @author VTeam
 * @version 0.0.1a
 * 
 * @see base UserControl component for a authorize users 
 */

namespace system\core;

use system\models\User as UserModel;
use user\components\Security;
use ext\CoreComponent;
use Exception;

/**
 * @link	
 * @license	
 * */
class User extends CoreComponent
{
    /** @var Register compon  ents list */
    private $_userSession = array();

    /** @var User Data storage */
    private $Storage = FALSE;
    
    /** @var User user model */
    private $_userModel = -1;
    
    /** @var Key for storge */
    private $storageKey = 'userSession';

    /** @var  default array for a system user */
    private $defaultStorageArray = array(
        'id' => 1000,
        'identify' => 'System',
        'idsession' => ''
    );
   
    /**
     * construct
     * set default values 
     * */
    public function __construct()
    {
        $this->Storage = \Yii::$app->getSession();
    }
    
    public function init() {
        parent::init();
        //default protect
    }
    
    public function login($model)
    {
        $UserResult = UserModel::find()->where(['user_identify'=>$model->username])->one();
        
        if (!$UserResult)
        {
            $this->logout();
            return FALSE;
        }
        
        $passwordHash = \vlibrary\util\Security::makeHash($model->password);

        if ($passwordHash === $UserResult->user_password)
        {
            $storageArray = array(
                'id' => $UserResult->id,
                'identify' => $UserResult->user_name,
                'idsession' => $passwordHash
            );

            $this->Storage->set($this->storageKey, $storageArray);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Guest array data
     * 
     * @return	array 	default guest 	
     * */
    public function logout()
    {
        $this->Storage->set($this->storageKey, $this->defaultStorageArray);
        return TRUE;
    }

    /**
     * Checks whether the user is authorized
     * 
     * @return	bool	TRUE or FALSE 
     * */
    public function isGuest()
    {
        $userIdentifyArray = $this->Storage->get($this->storageKey, FALSE);

        if ($userIdentifyArray)
        {
            if ($userIdentifyArray['id'] === 1000)
            {
                return TRUE;
            }
        }
        else
        {
            return $this->logout();
        }
        return FALSE;
    }

    /**
     * Return user information 
     * 
     * @return 	object		AR object model user
     * */
    public function getUser()
    {
        if ($this->_userModel===-1){
             $this->_userModel=UserModel::find()->where(['id'=>  $this->getId()])->one();
        }
        
        return $this->_userModel;
    }

    /**
     * Return id user 
     * (default (if not auth) return 1000) 
     * 
     * @return 	int 	user id
     * */
    public function getId()
    {
        $userIdentifyArray = $this->Storage->get($this->storageKey, array());
        if (array_key_exists('id', $userIdentifyArray))
            return $userIdentifyArray['id'];
        else
            return 1000;
    }

}

/* file  */
/* End of file */