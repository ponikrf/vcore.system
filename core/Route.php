<?php

/**
 * Url helper component
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace system\core;

use yii\base\Object;

/**
 * @link
 * @license
 * */
class Route extends Object {

    public $module;
    public $controller;
    public $action;

    public $route;
    
    /**
     * start prefix of path
     */
    public $startPrefix = '';

    /**
     * end prefix of path
     */
    public $endPrefix = '';

    /**
     * Basic params for url
     */
    public $params = [];

    
    public function init() {
        parent::init();

        $resolveArray = \Yii::$app->getRequest()->resolve();

        if (empty($resolveArray[0])) {
            $routePath = \Yii::$app->defaultRoute;
        } else {
            $routePath = $resolveArray[0];
        }
        
        $this->route = $routePath;
        
        $routeArray = explode('/', $routePath);
        
        if (count($routeArray)!=3){
            throw new \Exception("Bad request route path - ".$routePath);
        }
        
        $this->module=$routeArray[0];
        $this->controller=$routeArray[1];
        $this->action=$routeArray[2];
    }

    public function getRoute(){
        return $this->route;
    }

    public function make($action = '', $params = []) {
        $path = [$this->startPrefix . $action . $this->endPrefix];

        if (!empty($params)) {
            $params = array_merge($this->params, $params);
        } else {
            $params = $this->params;
        }

        return array_merge($path, $params);
    }

    /**
     * Set bascic params
     */
    public function setParams($paramArray = []) {
        $this->params = $paramArray;
    }

    /**
     * Set start prefix
     */
    public function setStartPrefix($prefix) {
        $this->startPrefix = $prefix;
    }

    /**
     * Set end prefix
     */
    public function setEndPrefix($prefix) {
        $this->endPrefix = $prefix;
    }

}
