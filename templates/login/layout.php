<?php
use yii\helpers\Html;
use app\assets\AppAsset;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->Template->title) ?></title>        
	<link type="text/css" rel="stylesheet" href="css/login.css">
        <?php $this->head() ?>        
</head>
<body>
     <?php $this->beginBody() ?>
	<form class="form-signin" action="" method="POST">
		<?php echo \system\widgets\FlashAlert::widget(); ?>
		<?php echo $content; ?>
	</form>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>