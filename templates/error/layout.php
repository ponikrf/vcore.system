<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->Template->title) ?></title>
        <link type="text/css" rel="stylesheet" href="css/error.css">
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="protect-container">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php
                    $linksArray[] = array('label' => 'На главную', 'url' => array('/'));
                    $linksArray[] = array('label' => 'Обновить', 'url' => '');

                    echo Nav::widget(
                            array(
                                'options' => ['class'=>'nav nav-list nav-justified'],
                                'items' => $linksArray
                            )
                    );
                    ?>
                    <hr>
                    <div class="information-container">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>