<?php
return array(
    'layout' => '@system/templates/general/layout',
    'title' => '',
    'BreadCrumbs' => new \vlibrary\base\BreadCrumbStorage(),
    'extraLeftNav' => '',
    'extraRightNav' => '',
    #'generalNav' => '@system/views/elements/generalNav',
    
    /*  Layout information	 */
    'startPage' => '',
    'headerContent' => '',
    'leftContent' => '',
    'rightContent' => '',
    'preContent' => '',
    'preContentNavigation' => '',
    'postContent' => '',
);
?>