<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="/css/navigation.css" rel="stylesheet">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->Template->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php

            NavBar::begin(array(
                            'options' => array(
                                    'class' => ' navbar navbar-default navbar-static-top',
                            ),
            ));

            echo Nav::widget(array(
                    'options' => array('class' => 'navbar-nav'),
                    'items' => array(
                        array('label' => 'Главная','url' => ['/']),
                        
                        array('label' => 'Справочники','items'=>[
                            ['label' => 'Системные','url'=>['/catalog/partition/list']],
                        ]),
                        
                        array('label' => 'Пользователи','items' => [
                            ['label' => 'Управление','url'=>['/system/user/list']],
                            ['label' => 'Разрешения','url'=>['/system/user-permission/list']],
                            ['label' => 'Роли','url'=>['/system/user-role/list']],
                            ['label' => 'Должности','url'=>['/system/user-position/list']],
                            ['label' => 'Выход','url'=>['/system/user/logout']],
                        ]),

                        array('label' => 'Система','items' => [
                            ['label' => 'Базовые таблицы','url'=>['/system/base-table/index']],
                        ]),
                        
                    ),
            ));

            NavBar::end();
        ?>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo yii\widgets\Breadcrumbs::widget(array('links' => $this->Template->BreadCrumbs->getArray())); ?>
                </div>
            </div>
        </div>
        
        <?php $this->beginBody() ?>        
	<div class="container">
		<?php if ($this->Template->headerContent): ?>
			<div class="row"><div class="col-md-12"><?php echo $this->Template->headerContent; ?></div></div>	
		<?php endIf ?>
		<div class="row">
			
			<?php if ($this->Template->leftContent): ?>
				<div class="col-md-3"><?php echo $this->Template->leftContent; ?></div>	
			<?php endIf ?>
			
			<?php
					$templateColum = 12;
				if (($this->Template->leftContent != '' AND $this->Template->rightContent != ''))
					$templateColum = 6;
				else if (($this->Template->leftContent != '' OR $this->Template->rightContent != ''))
					$templateColum = 9;
			?>
			
			<div class="col-md-<?php echo $templateColum; ?>"> 
				<?php  
					echo $this->Template->preContent;
					echo \system\widgets\FlashAlert::widget();
					echo $this->Template->preContentNavigation;
					echo $content; 
					
					echo $this->Template->postContent; 
				?>
			 </div>
			
			<?php if ($this->Template->rightContent): ?>
				<div class="col-md-3"><?php echo $this->Template->rightContent; ?></div>	
			<?php endIf ?>
		</div>
	</div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>        