<?php

/**
 * Template widget
 * ===============
 * 
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace system\widgets;

use system\components\UserControl;


use yii\base\Widget;
use yii\helpers\Html;

/**
 * @link
 * @license
 * */
class ButtonNotify extends Widget {

    public $label = 'Button';
    public $context = 'default';
    public $notify = 'Notify?';
    public $size = FALSE;
    public $url = NULL;
    public $submit = FALSE;
    public $options = [];
    public $icon;
    public $action;

    public function init() {
        parent::init();
        if ($this->size) {
            $this->size = 'btn-' . $this->size;
        }
        $this->options = array_merge($this->options, ['data-confirm' => $this->notify, 'data-method' => "post",]);
        Html::addCssClass($this->options, 'btn btn-' . $this->context . ' ' . $this->size);
    }

    public function run() {
        if ($this->action) {
            if (!UserControl::checkAction($this->action)) {
                return '';
            }
        }

        if ($this->url)
            echo Html::a($this->renderIcon($this->icon) . ' ' . $this->label, $this->url, $this->options);
        else if ($this->submit)
            echo Html::submitButton($this->label, $this->options);
        else
            echo Html::button($this->label, $this->options);
    }

    public function renderIcon($icon) {
        return Icon::widget(['icon' => $icon]);
    }

}
