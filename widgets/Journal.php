<?php

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Journal extends Widget {

    public $baseClass = 'journal';
    public $content;
    public $options;

    public function init() {
        Html::addCssClass($this->options, $this->baseClass);
        echo Html::beginTag('div', $this->options);
        echo $this->content;
    }

    /**
     * end
     */
    public function run() {
        echo Html::endTag('div');
    }

}
