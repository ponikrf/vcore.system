<?php

namespace system\widgets;

use yii\base\Widget;

/**
 * Alert widget
 * Use basic bootstrap alert class  
 * 
 * @author 	Tar
 * @link 	http://twitter.github.io/bootstrap/components.html#alerts
 * @example  app\widgets\Alert::widget(array('messageBody' => 'Alert text'));
 * 
 * all parameters
 * $alert = array(
 * 		'headerBody' 		=> '', 
 * 		'messageBody'	 	=> '',
 * 		'divclass'			=> '',
 * 		'divExternal'		=> '',
 * )
 * 
 * */
class FlashAlert extends Widget
{
	/**
	 * print alert message
	 * @see bootstrap help
	 * @return null
	 * */
	public function init()
	{
		$Alert = \Yii::$app -> getSession() -> getFlash('publicAlert',NULL);
		
		if (!empty($Alert))
		{
				
			$alertText = '';
			
			if ($Alert['notifyHeader'])
			{
				$alertText = '<h3>'.$Alert['notifyHeader'].'</h3>';
			} 
			
			$alertText .= $Alert['notify'];
			
			echo \yii\bootstrap\Alert::widget(array('body' => $alertText,'options'=>array('class'=>'alert-'.$Alert['status'])));
		}
	}
}
