<?php

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Label extends Widget {
    
    public $baseClass = 'label';
    public $context = 'default';
    public $content;
    public $options;
    
    public function init() {
        Html::addCssClass($this->options,$this->baseClass.($this->context?' label-'.$this->context:''));
        echo Html::beginTag('span', $this->options);
        echo $this->content;
    }

    public function run() {
        echo Html::endTag('span');
    }

}
