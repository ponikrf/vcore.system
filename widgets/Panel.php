<?php

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * 
 * */
class Panel extends Widget {
    
    public $baseClass = 'panel';
    public $context = 'default';
    public $content;
    public $options;
    
    public function init() {
        Html::addCssClass($this->options,$this->baseClass.($this->context?' panel-'.$this->context:''));
        echo Html::beginTag('div', $this->options);
        echo $this->content;
    }

    public function run() {
        echo Html::endTag('div');
    }

}
