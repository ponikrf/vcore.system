<?php

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class FormBlock extends Widget {

    public $baseClass = 'form-block';
    public $header;
    public $content;
    public $options;

    public function init() {
        Html::addCssClass($this->options, $this->baseClass);
        echo Html::beginTag('div', $this->options);
        if ($this->header){
            echo Html::tag('h3', $this->header);
        }
        echo $this->content;
    }

    public function run() {
        echo Html::endTag('div');
    }

}
