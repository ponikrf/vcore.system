<?php

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use system\widgets\Button;
use system\widgets\ButtonGroup;
use system\widgets\ButtonNotify;

class JournalView extends Widget {

    public $dataProvider;
    public $objectName = 'id';
    public $printPager = TRUE;
    public $readOnly = FALSE;
    public $journalContentAction;
    public $journalContent;

    public function init() {
        $readOnly = $this->readOnly;
        if (!$this->journalContentAction) {
            $this->journalContentAction = function ($Model) use ($readOnly) {
                if (!$readOnly) {
                    ButtonGroup::begin(['options' => ['class' => 'pull-right']]);
                    echo Button::widget(['label' => 'Редактировать', 'icon' => 'edit', 'context' => 'warning', 'action'=>'edit', 'size' => 'xs', 'url' => \Yii::$app->Route->make('edit', ['id' => $Model->id])]);
                    echo ButtonNotify::widget(['label' => 'Удалить', 'icon' => 'remove', 'context' => 'danger', 'action'=>'delete', 'size' => 'xs', 'url' => \Yii::$app->Route->make('delete', ['id' => $Model->id]), 'notify' => 'Удалить?']);
                    ButtonGroup::end();
                }
            };
        }
        if (!$this->journalContent) {
            $this->journalContent = function ($Model) {
                echo \system\widgets\JournalRecord::widget(['Model' => $Model, 'route' => \Yii::$app->Route->make('show', ['id' => $Model->id])]);
            };
        }
    }

    /**
     * end
     */
    public function run() {
        if (!$this->dataProvider instanceof \yii\data\DataProviderInterface) {
            throw new Exception("Data provader not set");
        }

        $Models = $this->dataProvider->getModels();

        $ContentAction = $this->journalContentAction;
        $Content = $this->journalContent;

        foreach ($Models as $Model) {
            JournalContainer::begin();
            JournalContent::begin();
            $ContentAction($Model);
            $Content($Model);
            JournalContent::end();
            JournalContainer::end();
        }
        $this->pager();
    }

    public function pager() {
        if ($this->printPager) {
            echo \yii\widgets\LinkPager::widget([
                'pagination' => $this->dataProvider->getPagination(),
            ]);
        }
    }

}
