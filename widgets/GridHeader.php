<?php
/**
 * Object header
 * =============
 * 
 * Header for object container
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * @link
 * @license
 * */

class GridHeader extends Widget
{
    public $baseClass = 'journal-header';
    public $content;
    public $options;
    
    public function init() {
        Html::addCssClass($this->options,$this->baseClass);
        echo Html::beginTag('div', $this->options);
        echo $this->content;
    }

    public function run() {
        echo Html::endTag('div');
    }
}
