<?php

namespace system\widgets;

use yii\base\Widget;

/**
 * 
 * */
class JournalRecord extends Widget {
    
    public $route;
    
    public $Model;

    public function init() {
        $short = '';
        $name = '';
        if ($this->Model->name_short){
            $short = '('.$this->Model->name_short.')';
        }
        
        if ($this->Model->name){
            $name = ' [<small>'.$this->Model->name.'</small>] ';
        }
        if ($this->Model->name_lang) {
            $url = "<h4>" . $this->Model->name_lang.$short.$name. "</h4>";
            echo \yii\helpers\Html::a($url, $this->route);
        }
        
        if ($this->Model->description) {
            echo "<p>" . $this->Model->description;
        }
        
    }

    public function run() {
        if ($this->Model->description) {
            echo "</p>";
        }
    }

}
