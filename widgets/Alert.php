<?php

/**
 * Template widget
 * ===============
 * 
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * @link
 * @license
 * */
class Alert extends Widget {

    public $content = '';
    public $context = 'default';
    public $options = [];
    
    public function init() {
        parent::init();
        Html::addCssClass($this->options, 'alert alert-'.$this->context);
        echo Html::beginTag('div',  $this->options);
    }

    public function run() {
        echo Html::endTag('div');
    }
    
}
