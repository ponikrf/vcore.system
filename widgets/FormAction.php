<?php

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class FormAction extends Widget {

    public $baseClass = 'form-group form-action';
    public $content;
    public $options;

    public function init() {
        Html::addCssClass($this->options, $this->baseClass);
        echo Html::beginTag('div', $this->options);
        echo $this->content;
    }

    public function run() {
        echo Html::endTag('div');
    }

}
