<?php

/**
 * Icon widget
 * -----------
 * 
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace system\widgets;

use yii\base\Widget;

/**
 * @link
 * @license
 * */
class Icon extends Widget {
    public $icon = FALSE;
    public $options = [];
    /**
     * end
     */
    public function run() {
        if ($this->icon) {
            echo "<span class=\"glyphicon glyphicon-" . $this->icon . "\"></span>";
        }
    }

}
