<?php

namespace system\widgets;

use yii\base\Widget;
use \yii\helpers\Html;

/**
 * 
 * */
class PanelFooter extends Widget {
    
    public $baseClass = 'panel-footer';
    public $content;
    public $options;
    
    public function init() {
        Html::addCssClass($this->options,$this->baseClass);
        echo Html::beginTag('div', $this->options);
        echo $this->content;
    }

    public function run() {
        echo Html::endTag('div');
    }

}
