<?php
/**
 * Template widget
 * ===============
 * 
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace system\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * @link
 * @license
 * */

class JournalContainer extends Widget
{
    public $baseClass = 'journal-container';
    public $content;
    public $options;
    
    public function init() {
        Html::addCssClass($this->options,$this->baseClass);
        echo Html::beginTag('div', $this->options);
        echo $this->content;
    }

    public function run() {
        echo Html::endTag('div');
    }
}
