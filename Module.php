<?php

namespace app\modules\system;

use system\helpers\RuntimeHelper;

class Module extends \ext\base\Module {

    public function init() {
        $this->controllerNamespace = "system\controllers";
        parent::init();
        
        
    }
   
    public function registers() {
        
        \system\helpers\MoneyHelper::addConfig('default', new \system\components\MoneyFormat());

        return [
            'CoreComponents' => ['class' => 'system\registers\CoreComponents'],
            'Externals' => ['class' => 'system\registers\Externals'],
            'Templates' => ['class' => 'system\registers\Templates'],
            'Relation' => ['class' => 'system\registers\Relation'],
            'TextEntity' => ['class' => 'system\registers\TextEntity'],
            'Scenarios' => ['class' => 'system\registers\Scenarios'],
            'AccessEntity' => ['class' => 'system\registers\AccessEntity'],
        ];
    }

    public function registerRelation()
    {
        return [
            'system'
        ];
    }
    
    public function registerCoreComponents() {
        return [
            'Template' => ['class' => 'system\core\Template'],
            'Route' => ['class' => 'system\core\Route'],
            'User' => ['class' => 'system\core\User'],
        ];
    }

    public function registerTemplates() {
        return [
            'general' => '@system/templates/general/template',
            'error' => '@system/templates/error/template',
            'login' => '@system/templates/login/template',
        ];
    }

    public function registerTextEntity() {
        return [
            'system' => function ($param) {
                return \yii\helpers\Html::a($param['id'],[
                    'system/default/index',
                    'system'=>$param['id'],
                ]);
            },
            'user' => function ($param) {
                return \yii\helpers\Html::a($param['id'],[
                    'system/user/show',
                    'id'=>$param['id'],
                ]);
            },                    
        ];
    }
    
    public function registerAccessEntity(){
        
       return [
           'label' => 'Система',
            
           'entitys' => [

                'base_table' => [
                    'actions'=>['add','update'],
                    'label' => 'Базовые таблицы',
                ],               
               
                'user' => [
                    'actions'=>['show','add','edit','close','change_password'],
                    'label' => 'Пользователи',
                ],
                
                'user_role' => [
                    'actions'=>['show','add','edit','delete'],
                    'label' => 'Роли',
                ],

                'user_position' => [
                    'actions'=>['show','add','edit','delete'],
                    'label' => 'Должности',
                ],
               
                'user_permission' => [
                    'actions'=>['show','edit'],
                    'label' => 'Разрешения',
                ],
            ],
             
            'actions' => [
                'show'            => 'Просмотр',
                'add'             => 'Добавление',
                'edit'            => 'Редактирование',
                'update'          => 'Обновление',
                'delete'          => 'Удаление',
                'close'           => 'Закрытие',
                'change_password' => 'Смена пароля',
            ],
       ]; 
    }
    
    public function registerScenarios(){
        
        return [
            'information' => function ($record){
                $file = \Yii::getAlias('@system/scenarios/record/'.$record.'.md');
                if (!file_exists($file)) throw new \Exception("Record file [$file] for information scenarios not found");

                \Yii::$app->Template->setTemplate('error');
                echo \Yii::$app->Template-> renderHtml(\yii\helpers\Markdown::process(file_get_contents($file)),[]);
                RuntimeHelper::end();
            },
            
            'toHome' => function (){
                \Yii::$app->Response->redirect(\yii\helpers\Url::toRoute('/'))->send();
                RuntimeHelper::end();
            },

            'toRoute' => function ($url){
                \Yii::$app->Response->redirect(\yii\helpers\Url::toRoute($url))->send();
                RuntimeHelper::end();
            },           
            
            'toUrl' => function ($url){
                \Yii::$app->Response->redirect($url)->send();
                RuntimeHelper::end();
            },

            'toLogin' => function (){
                \Yii::$app->Response->redirect(\yii\helpers\Url::toRoute(['/system/user/login']))->send();
                RuntimeHelper::end();
            },           
                    
            'refresh' => function (){
                \Yii::$app->Response->refresh()->send();
                RuntimeHelper::end();
            },
                    
            'return' => function (){
                if (!($url=\Yii::$app->Request->getReferrer())){
                    $url = \yii\helpers\Url::toRoute('/');
                }
                \Yii::$app->Response->redirect($url)->send();
                RuntimeHelper::end();
            },
        ];
    }
    
    
}
