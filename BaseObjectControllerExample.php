<?php
/**
 * For detal
 * @See \system\controllers\BaseObjectController
 */

namespace modulename\controllers;

use system\components\Scenario;
use system\components\UserControl;
use ObjectModel AS Model;


class ObjectNameController extends \system\controllers\BaseObjectController {

    public $Model;
    public $objectName = 'ObjectName';
    public $objectsName = 'ObjectNames';
    public $readOnly = FALSE;    
    public $listRoute = ['/system/user-position/list'];
    public $baseRoute = '/system/user-position/';
    
    public function init() {
        $this->Model = new Model();
        $this->Template->setTemplate('general');
        
        UserControl::setEntity('{module name}', '{access entity}');
        
        parent::init();
    }

    public function getModel($id) {
        if (!$Model = Module::find()->where(['id' => $id])->one()) {
            Scenario::run('information', ['objectNotFound']);
        }
        return $Model;
    }
}
