<?php

/**
 * User password security class
 *
 * @author VTeam
 * @version 0.0.1a
 */

namespace vlibrary\util;

use Exception;

/**
 * @link
 * @license
 * */

class Security
{
    /**
     * @var string for a generate random salt
     * */
    public static $CharString = 'abcdefghijklmnopqrstuvwxyz1234567890';

    /**
     * Generate random salt for a save to database
     * 
     * @return 	string		random salt 
     * */
    public static function generateSalt($lenght = 9)
    {
        $salt = '';
        $chars = strlen($lenght);

        for ($i = 0; $i < $lenght; $i++)
        {
            $salt .= self::$CharString[rand(0, $chars - 1)];
        }
        return $salt;
    }

    public static function makeHash($password, $salt = '', $secretString = 'vsecurity')
    {
        return md5(md5($salt . $password . $secretString));
    }

}

/* file  */
/* End of file */