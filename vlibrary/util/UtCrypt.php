<?php

namespace vlibrary\util;

use yii\base\Component;

class UtCrypt extends Component
{

    private static $S;
    private static $x = 0;
    private static $y = 0;
    private static $ea = array('Error input data format', 'Key should not be longer than 255 bit');

    private static function gk($k)
    {
	static::$S = array();
	static::$x = 0;
	static::$y = 0;
	$k = unpack('C*', $k);
	array_unshift($k, array_shift($k));
	$kl = count($k);
	for ($i = 0; $i < 256; $i++)
	{
	    static::$S[$i] = $i;
	}$j = 0;
	for ($i = 0; $i < 256; $i++)
	{
	    $j = ($j + static::$S[$i] + $k[$i % $kl]) % 256;
	    static::asw($i, $j);
	}
    }

    private static function asw($i1, $i2)
    {
	$t = static::$S[$i1];
	static::$S[$i1] = static::$S[$i2];
	static::$S[$i2] = $t;
    }

    private static function ki()
    {
	static::$x = (static::$x + 1) % 256;
	static::$y = (static::$y + static::$S[static::$x]) % 256;
	static::asw(static::$x, static::$y);
	return static::$S[(static::$S[static::$x] + static::$S[static::$y]) % 256];
    }

    private static function cy($d)
    {
	$c = '';
	for ($m = 0; $m < strlen($d); $m++)
	{
	    $c .= $d[$m] ^ chr(static::ki());
	}
	return $c;
    }

    private static function pt($d, $k)
    {
	if ((!is_string((string) $d)) OR ( !is_string((string) $k)))
	    throw new Exception(static::$ea[0]);
	if (strlen($k) > 254)
	    throw new Exception(static::$ea[1]);
    }

    public static function reCrypt($data, $key)
    {
	static::gk($key);
	return static::cy($data);
    }

}
