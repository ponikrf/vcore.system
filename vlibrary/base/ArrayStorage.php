<?php
/**
 * Base storage class
 * 
 * @author VTeam
 * @version 0.0.1a
 */
 
namespace vlibrary\base;

use \Exception;

/**
 * @link
 * @license
 * */ 

class ArrayStorage extends \yii\base\Object
{
	/**
	 * @var for a local storage 
	 * */
	private $_localStorage = array();

	/**
	 * Set var to local storage
	 * 
	 * @param	string	key for local storage
	 * @return	bool	replace? (if replace return true)
	 * */
	public function set($key,$value)
	{
		$return = $this -> has($key);
		$this -> _localStorage[$key] = $value;
		return $return;
	}

	/**
	 * Unset value of a key from array
	 * 
	 * @param 	string		key of local storage 
	 * @return 	null
	 * */
	public function uset($key)
	{
		if ($this->has($key)) 
		{
			unset($this->_localStorage[$key]);
		}
	}

	/**
	 * Has a key
	 * 
	 * @param 	string 		key of local storage
	 * @return 	bool		TRUE or FALSE if key not exist
	 * */
	public function has($key)
	{
		return array_key_exists($key, $this -> _localStorage);
	}
	
	/**
	 * Return the key value
	 * 
	 * @param 	string		key of local storage
	 * @return	mixed		storage content
	 * */
	public function get($key)
	{
		if (!$this->has($key)) 
		{
			throw new Exception("Error of local storage - var [{$key}] not found.");
		}
		return $this->_localStorage[$key];
	}

	/**
	 * Return local storage original array
	 * 
	 * @access 		public 
	 * @return 		array	
	 * */
	public function getArray()
	{
		return $this -> _localStorage; 
	}

	/**
	 * Set array for a replace local storage array
	 * 
	 * @return	null
	 * */
	public function setArray($storageArray)
	{
		if (!is_array($storageArray))
		{
			throw new Exception("Error of local storage - var for replace local storage not array");			
		}
		$this -> _localStorage = $storageArray;
	}

	/**
	 * Return a local storage key list
	 * 
	 * @return	array	key list array
	 * */
	public function getKeyList()
	{
		$varname_list = array(); 
		foreach ($this -> _localStorage as $key => $v) 
		{
			$varname_list[] = $key;
		}
		
		return $varname_list;
	}
}

