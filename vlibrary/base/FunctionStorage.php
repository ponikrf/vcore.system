<?php
/**
 * Helper function storage class %-)
 * 
 * @author VTeam
 * @version 0.0.1a
 */
 
namespace vlibrary\base;

use \Closure;
use \Exception;

/**
 * @link
 * @license
 * */ 
class FunctionStorage 
{
	/**
	 * @var Local callback function storage
	 * */
	private $_localFunctions = array();
	
	/**
	 * Add callback function to storage
	 * 
	 * @access 		public
	 * @param 		string 			function name (virtual function name - unlimited)
	 * @param 		closure 		function
	 * @return 		bool			
	 * */
	public function addFunction($functionName,Closure $functionCallBack)
	{
		$return = FALSE;
		
		if (array_key_exists($functionName,$this -> _localFunctions))
			$return = TRUE;
				
		if (is_callable($functionCallBack))
			$this -> _localFunctions[$functionName] = $functionCallBack;
		else
			throw new Exception("Function '{$functionCallBack}' can`t callable.");
		return $return;
	}

	/**
	 * Run function from storage
	 * 
	 * @access 		protected
	 * @param 		string 			function name (virtual function name - unlimited) from storage
	 * @param 		array 			array params for function 
	 * @return 		null 	
	 * */
	public function execFunction($functionName,$functionParams = array())
	{
		if (!isset($this -> _localFunctions[$functionName]))
			throw new Exception("Function '{$functionName}' not found in the storage.");
		
		if (is_callable($this -> _localFunctions[$functionName]))
			return call_user_func_array($this -> _localFunctions[$functionName], $functionParams);
	}
	
	/**
	 * Has function?
	 * 
	 * @access 		protected
	 * @param 		string 			function name (virtual function name - unlimited) from storage
	 * @param 		array 			array params for function 
	 * @return 		null 	
	 * */
	public function hasFunction($functionName)
	{
		if (isset($this -> _localFunctions[$functionName]))
			return TRUE;
		return FALSE;
	}

        public function getFunction($functionName){
            if (isset($this -> _localFunctions[$functionName]))
		return $this -> _localFunctions[$functionName];
        }


        /**
	 * Magic bleat
	 *  
	 * @access 		public  
	 * @param 		string 			function name
	 * @param 		array 			array params for function
	 * @return 		mixed			return result runing function execFunction 	
	 * */
	public function __call($functionName,$functionParams = array())
	{
		return $this -> execFunction($functionName,$functionParams);
	}
}