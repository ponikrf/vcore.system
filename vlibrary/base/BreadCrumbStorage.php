<?php
/**
 * BreadCrumbs class
 *  
 * @author VTeam
 * @version 0.0.1a
 */
 
namespace vlibrary\base;
 
/**
 * @link
 * @license
 * */ 

class BreadCrumbStorage {

	/**
	 * @var bread crumbs array
	 */
	protected $breadCrumbs = array();

	/**
	 * Get bread crumbs array
	 * @access	public
	 * @return  string
	 */
	public function getArray()
	{
		return $this -> breadCrumbs;
	}
	
	/**
	 * Add trail
	 * @access 	public
	 * @param 	array   	trail params
	 * @return 	NULL
	 * */
	public function add($trailParams = '')
	{
		
		if (empty($trailParams) OR !is_array($trailParams))	
			return NULL;

		$this -> breadCrumbs[] = $trailParams;
                return TRUE;
	}

	/**
	 * Back to previous trail
	 * @access public
	 * @return NULL
	 * */
	public function back()
	{
		array_pop($this -> breadCrumbs);
		end ( $this -> breadCrumbs );
	}

	/**
	 * Clean bread crumb array
	 * @access public
	 * @return NULL
	 * */
	public function clean()
	{
		$this -> breadCrumbs = array();
	}

}
 
/*End of file VCSV.php*/
/*Location: application/components/VCSV.php*/