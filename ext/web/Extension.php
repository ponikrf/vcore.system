<?php

namespace ext\web;

use \Yii\Base\Object;

class Extension extends Object
{

    public function __get($getName)
    {
	return \Yii::$app->$getName;
    }

}

?>