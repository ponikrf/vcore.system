<?php

/**
 * Default controller
 * ==========================
 * 
 * general function
 * -----------------
 * 
 * * index
 * 
 * @author VTeam
 * @version 0.0.1a
 * */

namespace ext;

use yii\i18n\Formatter as YiiFormatter;

/**
 * @link
 * @license
 * */
class Formatter extends YiiFormatter {

    public function asObject($id, $class = 'default',$field = 'name_lang'){
        return $class::find()->where(['id'=>$id])->one()->$field;
    }
    
    public function asMoney($money, $format = 'default') {
        return \Yii::$app->MoneyManager->renderMoney($money, TRUE, $format);
    }

    public function asMoneyclear($money, $format = 'default') {
        if ($money) {
            return \Yii::$app->MoneyManager->renderMoney($money, TRUE, $format);
        } else {
            return '';
        }
    }

    public function asStatus($status, $format = ['OFF', 'ON']) {
        if ($status) {
            return $format[1];
        } else {
            return $format[0];
        }
    }

    public function asDate($date, $format = 'short') {
        if (!$date) {
            return NULL;
        }
        
        return \system\helpers\DateTimeHelper::toDate(strtotime($date));
    }

    public function asUser($user, $format = 'short') {
        if ($model = \user\models\User::find()->where(['id'=>$user])->one()){
            return $model->user_full_name;
        }
        return '';
    }

    public function asDatetime($datetime, $format = 'short') {
        if (!$datetime) {
            return NULL;
        }
        $datetime = parent::normalizeDatetimeValue($datetime);
        return \vlibrary\helpers\DateTime::toDatetime($datetime);
    }

    public function asCatalog($catalog, $format = 'name') {
        if ($catalog) {
            $catalog = \catalog\components\SystemCatalog::getSection($catalog);

            if ($catalog) {
                $catalog = $catalog->catalog_name_lang;
            }
        }
        return $catalog;
    }

    public function asByte($byte, $format = FALSE) {
        return \vlibrary\helpers\Network::roundByte($byte);
    }

    public function asIpv4Address($ipBinary, $format = false) {
        $Network = new \internet\models\Ipv4Network();
        return $Network->binaryToIp($ipBinary);
    }

    public function asReplace($string, $replace, $replaced) {
        return str_replace($replace, $replaced, $string);
    }

    public function asEmpty($string){
        if (!$string){
            return ' ';
        }
        return $string;
    }
}
