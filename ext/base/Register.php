<?php
/**
 * Class extension system yii controller class
 * */
namespace ext\base;

class Register extends Storage
{
	/**
	 * Default register function 
	 * 
	 * @param	array	register array
	 * @param	string	module name
	 * @return	null
	 */
	public function register(array $registerArray,$moduleName)
	{
		$this -> set($moduleName,$registerArray);
	}
}