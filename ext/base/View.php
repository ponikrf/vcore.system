<?php

namespace ext\base;

class View extends \yii\web\View
{

    public function __get($name)
    {
	return \Yii::$app->$name;
    }

}
