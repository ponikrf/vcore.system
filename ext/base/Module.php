<?php

/**
 * Class Module 
 * Extend Yii module class
 * 
 * @author tar
 * */

namespace ext\base;

use yii\base\Module as YiiModule;

class Module extends YiiModule{}

?>