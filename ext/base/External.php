<?php
/**
 * Class extension system yii controller class
 * */

namespace ext\base;

use yii\web\Controller as YiiController;

class External extends YiiController
{

    /**
     * Base getter
     * Replace parent controller class getter
     * 
     * @access 	public
     * @param 	string
     * @return 	mixed	getter return
     * */
    public function __get($getName)
    {
	return \Yii::$app->$getName;
    }

    /**
     * base caller
     * for a yii functions
     */
    public function __call($name, $params)
    {
	return call_user_func_array(array(\Yii::$app, $name), $params);
    }

}
