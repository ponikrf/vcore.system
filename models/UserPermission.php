<?php

namespace system\models;

use Yii;

/**
 * This is the model class for table "vc_user_permission".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_user_role
 * @property string $module
 * @property string $entity
 * @property string $action
 * @property string $create_datetime
 *
 * @property VcUserRole $idUserRole
 * @property VcUser $idUser
 */
class UserPermission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vc_user_permission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_user_role'], 'integer'],
            [['module', 'entity', 'action'], 'required'],
            [['create_datetime'], 'safe'],
            [['module', 'entity', 'action'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Инициатор',
            'id_user_role' => 'Роль',
            'module' => 'Модуль',
            'entity' => 'Сущность',
            'action' => 'Действие',
            'create_datetime' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserRole()
    {
        return $this->hasOne(VcUserRole::className(), ['id' => 'id_user_role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(VcUser::className(), ['id' => 'id_user']);
    }
}
