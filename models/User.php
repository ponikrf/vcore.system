<?php

namespace system\models;

use Yii;

/**
 * This is the model class for table "vc_user".
 *
 * @property integer $id
 * @property integer $id_user_role
 * @property integer $id_user_position
 * @property string $user_identify
 * @property string $user_password
 * @property string $user_name
 * @property string $user_locale
 * @property string $start_datetime
 * @property string $end_datetime
 * @property string $create_datetime
 *
 * @property VcUserPosition $idUserPosition
 * @property VcUserRole $idUserRole
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vc_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user_role', 'id_user_position', 'user_identify', 'user_password', 'user_name'], 'required'],
            [['id_user_role', 'id_user_position'], 'integer'],
            [['start_datetime', 'end_datetime', 'create_datetime'], 'safe'],
            [['user_identify'], 'string', 'max' => 64],
            [['user_password'], 'string', 'max' => 32],
            [['user_name'], 'string', 'max' => 120],
            [['user_locale'], 'string', 'max' => 10],
            [['user_identify'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user_role' => 'Роль',
            'id_user_position' => 'Должность',
            'user_identify' => 'Идентификатор',
            'user_password' => 'Пароль',
            'user_name' => 'Имя пользователя',
            'user_locale' => 'Локализация',
            'start_datetime' => 'Дата начала',
            'end_datetime' => 'Дата конца',
            'create_datetime' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserPosition()
    {
        return $this->hasOne(VcUserPosition::className(), ['id' => 'id_user_position']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUserRole()
    {
        return $this->hasOne(VcUserRole::className(), ['id' => 'id_user_role']);
    }
}
