<?php

namespace system\models;

use Yii;

/**
 * This is the model class for table "vc_notify_action".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_lang
 * @property string $name_short
 * @property string $description
 *
 * @property VcNotify[] $vcNotifies
 */
class NotifyAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vc_notify_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_lang'], 'required'],
            [['description'], 'string'],
            [['name', 'name_short'], 'string', 'max' => 50],
            [['name_lang'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name_lang' => 'Читаемое название',
            'name_short' => 'Короткое название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVcNotifies()
    {
        return $this->hasMany(VcNotify::className(), ['id_notify_action' => 'id']);
    }
}
