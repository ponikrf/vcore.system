<?php

namespace system\models;

use Yii;

/**
 * This is the model class for table "vc_notify".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_application
 * @property integer $id_notify_level
 * @property integer $id_notify_action
 * @property string $notify_text
 * @property string $notify_data
 * @property string $create_datetime
 *
 * @property VcNotifyAction $idNotifyAction
 * @property VcApplication $idApplication
 * @property VcNotifyLevel $idNotifyLevel
 */
class Notify extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vc_notify';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_application', 'id_notify_level', 'id_notify_action'], 'integer'],
            [['create_datetime'], 'safe'],
            [['notify_text', 'notify_data'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Инициатор',
            'id_application' => 'Приложение',
            'id_notify_level' => 'Важность',
            'id_notify_action' => 'Действие',
            'notify_text' => 'Текст',
            'notify_data' => 'Данные',
            'create_datetime' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNotifyAction()
    {
        return $this->hasOne(VcNotifyAction::className(), ['id' => 'id_notify_action']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdApplication()
    {
        return $this->hasOne(VcApplication::className(), ['id' => 'id_application']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNotifyLevel()
    {
        return $this->hasOne(VcNotifyLevel::className(), ['id' => 'id_notify_level']);
    }
}
