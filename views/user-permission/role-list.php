<?php

use system\widgets\Journal;
use system\widgets\JournalHeader;

Journal::begin();

    JournalHeader::begin();
        echo 'Выбор роли';
    JournalHeader::end();
    
    echo \system\widgets\JournalView::widget([
        'readOnly' => TRUE,
        'dataProvider'=>$dataProvider,
        'journalContent' => function ($Model) {
            echo \system\widgets\JournalRecord::widget(['Model' => $Model, 'route' => \Yii::$app->Route->make('select-module', ['role' => $Model->id])]);
        },
    ]);

Journal::end();