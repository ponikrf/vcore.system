<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use system\widgets\FormAction;
use system\widgets\FormBlock;

use system\components\UserControl;

$Form = ActiveForm::begin();

FormBlock::begin(['header' => 'Модуль: '.$moduleLabel]);
foreach ($modelKeys as $key => $value) {

        echo $Form->field($Model, $key)->label($value)->checkboxList($checkBoxArray[$key], [
            'itemOptions' => [
                'labelOptions'=>[
                    'class' => 'checkbox-list'
                ],
            ]
        ]);

}
FormBlock::end();
if (UserControl::checkAction('edit')){
    FormAction::begin();
    echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
    FormAction::end();
}
ActiveForm::end();
