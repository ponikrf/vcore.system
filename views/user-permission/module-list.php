<?php

use system\widgets\Journal;
use system\widgets\JournalHeader;

Journal::begin();

    JournalHeader::begin();
        echo 'Выбор модуля';
    JournalHeader::end();

    echo \system\widgets\JournalView::widget([
        'readOnly' => TRUE,
        'dataProvider'=>$dataProvider,
        'journalContent' => function ($Model) use ($role) {
            echo \system\widgets\JournalRecord::widget(['Model' => $Model, 'route' => \Yii::$app->Route->make('permission',  ['role' => $role,'module'=>$Model->id])]);
        },
    ]);

Journal::end();