<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use system\widgets\FormAction;
use system\widgets\FormBlock;

$form = ActiveForm::begin();

FormBlock::begin(['header' => 'Создание таблицы']);
echo $form->field($Model, 'scheme')->input('text');
echo $form->field($Model, 'table')->input('text');
echo $form->field($Model, 'description')->textarea();
FormBlock::end();

FormAction::begin();
    echo Html::submitButton('Добавить', ['class' => 'btn btn-primary']);
FormAction::end();

ActiveForm::end();