<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use system\widgets\FormAction;
use system\widgets\FormBlock;

$Form = ActiveForm::begin();

FormBlock::begin(['header' => 'Смена пароля для ['.$Model->user_name.']']);
    echo $Form->field($Model, 'user_password')->input('password');
FormBlock::end();

FormAction::begin();
    echo Html::submitButton('Установить', ['class' => 'btn btn-danger']);
FormAction::end();

ActiveForm::end();
