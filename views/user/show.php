<?php
use yii\widgets\DetailView;
use system\widgets\DetailHeader;

use system\widgets\Button;
use system\widgets\ButtonGroup;
use system\widgets\ButtonNotify;

DetailHeader::begin(['content'=>'Пользователь системы']);
    $html = Button::widget(['label' => 'Смена пароля', 'action'=>'change_password','icon' => 'erase', 'context' => 'warning','size'=>'xs', 'url' => \Yii::$app->Route->make('change-password', ['id' => $Model->id])]);
    $html .= Button::widget(['label' => 'Редактирование', 'action'=>'edit', 'icon' => 'edit', 'context' => 'warning','size'=>'xs', 'url' => \Yii::$app->Route->make('edit', ['id' => $Model->id])]);
    $html .= Button::widget(['label' => 'Закрытие', 'action'=>'close', 'icon' => 'remove', 'context' => 'danger','size'=>'xs', 'url' => \Yii::$app->Route->make('close', ['id' => $Model->id])]);
    echo ButtonGroup::widget(['options' => ['class' => 'pull-right'], 'content' => $html]);
DetailHeader::end();
    
echo DetailView::widget([
    'model' => $Model,
    'formatter' => ['class' => '\ext\Formatter'],
    'attributes' => [
        ['attribute' => 'id'],
        ['attribute' => 'user_identify'],
        ['attribute' => 'user_name'],
        ['attribute' => 'id_user_role', 'format' => ['object',"\\system\\models\\UserRole"]],
        ['attribute' => 'id_user_position', 'format' => ['object',"\\system\\models\\UserPosition"]],
        ['attribute' => 'user_locale'],
        ['attribute' => 'create_datetime', 'format' => 'date'],
    ],
]);
