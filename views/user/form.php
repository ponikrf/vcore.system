<?php

use system\widgets\FormBlock;
/**
 * standart base object form
 */
FormBlock::begin(['header' => $objectName]);
echo $form->field($Model, 'user_identify')->input('text');
echo $form->field($Model, 'user_password')->input('text');
echo $form->field($Model, 'user_name')->input('text');
echo $form->field($Model, 'user_locale')->input('text');
FormBlock::end();
