<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use system\widgets\Panel;
use system\widgets\PanelHeader;
use system\widgets\PanelBody;
use system\widgets\PanelFooter;

Panel::begin();

        echo PanelHeader::widget(['content' => 'Авторизация']);

	PanelBody::begin();
		$form = ActiveForm::begin(['id' => 'login-form']); 
			 echo $form->field($model, 'username')->input('text'); 
			 echo $form->field($model, 'password')->passwordInput(); 
		ActiveForm::end();
	PanelBody::end();

	PanelFooter::begin();
		echo Html::submitButton('Вход',['class' => 'btn btn-primary']);
	PanelFooter::end();
Panel::end();