<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use system\components\BaseObject;

use system\widgets\FormAction;
use system\widgets\FormBlock;
use system\models\UserRole;
use system\models\UserPosition;

$Form = ActiveForm::begin();

FormBlock::begin(['header' => 'Новый пользователь']);

    echo $Form->field($Model, 'id_user_role')->dropDownList(BaseObject::getArray(UserRole::find()->all()));
    echo $Form->field($Model, 'id_user_position')->dropDownList(BaseObject::getArray(UserPosition::find()->all()));

    echo $Form->field($Model, 'user_name')->input('text');
    echo $Form->field($Model, 'user_identify')->input('text');
    echo $Form->field($Model, 'user_password')->input('password');
    echo $Form->field($Model, 'user_locale')->input('text');
    
FormBlock::end();

FormAction::begin();
    echo Html::submitButton('Добавить', ['class' => 'btn btn-primary']);
FormAction::end();

ActiveForm::end();
