<?php

use yii\grid\GridView;
use yii\grid\GridViewAsset;
use system\widgets\GridHeader;
use system\widgets\Button;
use system\widgets\ButtonGroup;
use system\widgets\ButtonNotify;

GridViewAsset::register($this);

$GetAction = function ($Model, $index, $qq) {
    $html = Button::widget(['label' => '', 'action'=>'change_password','icon' => 'erase', 'context' => 'warning', 'url' => \Yii::$app->Route->make('change-password', ['id' => $Model->id])]);
    $html .= Button::widget(['label' => '', 'action'=>'edit', 'icon' => 'edit', 'context' => 'warning', 'url' => \Yii::$app->Route->make('edit', ['id' => $Model->id])]);
    $html .= Button::widget(['label' => '', 'action'=>'close', 'icon' => 'remove', 'context' => 'danger', 'url' => \Yii::$app->Route->make('close', ['id' => $Model->id])]);
    return ButtonGroup::widget(['options' => ['class' => 'pull-right btn-group-justified'], 'content' => $html]);
};

$GetId = function ($Model, $index, $qq) {
    return \yii\helpers\Html::a($Model->id,\Yii::$app->Route->make('show', ['id' => $Model->id]));
};

GridHeader::begin();
    echo "Пользователи системы";
    ButtonGroup::begin(['options' => ['class' => 'pull-right']]);
        echo Button::widget(['label' => 'Добавить', 'icon'=>'plus','action'=>'add', 'size' => 'xs','context'=>'primary','url'=>\Yii::$app->Route->make('add')]);
    ButtonGroup::end();
GridHeader::end();

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'formatter' => ['class' => '\ext\Formatter'],
    'layout' => "{items}\n{pager}",
    'columns' => [
        ['attribute' => 'id','value' => $GetId,'format' => 'raw'],
        ['attribute' => 'user_identify'],
        ['attribute' => 'user_name'],
        ['attribute' => 'id_user_role', 'format' => ['object',"\\system\\models\\UserRole"]],
        ['attribute' => 'id_user_position', 'format' => ['object',"\\system\\models\\UserPosition"]],
        ['attribute' => 'user_locale'],
        ['attribute' => 'create_datetime', 'format' => 'date'],
        ['value' => $GetAction, 'attribute' => 'id', 'format' => 'raw', 'contentOptions' => ['class' => 'table-action'], 'header' => ''],
        
    ],
]);
