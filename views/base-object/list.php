<?php

use system\widgets\Journal;
use system\widgets\JournalHeader;
use system\widgets\JournalContainer;
use system\widgets\JournalContent;

use system\widgets\Button;
use system\widgets\ButtonGroup;
use system\widgets\ButtonNotify;

use yii\helpers\Html;


Journal::begin();

    JournalHeader::begin();
        if (!$readOnly){
            echo $objectsName;
            ButtonGroup::begin(['options'=>['class'=>'pull-right']]);
                echo Button::widget(['label' => 'Добавить','icon'=>'plus', 'context'=>'primary','action'=>'add', 'size' => 'xs','url'=>$this->Route->make('add')]);
            ButtonGroup::end();
        }else{
            echo $objectsName;
            echo \system\widgets\Label::widget(['context'=>'success','content'=>'READ ONLY','options'=>['class'=>'pull-right']]);
        }
        
    JournalHeader::end();
    
    echo \system\widgets\JournalView::widget([
        'dataProvider'=>$dataProvider
    ]);

Journal::end();

