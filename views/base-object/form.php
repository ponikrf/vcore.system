<?php

use system\widgets\FormBlock;
/**
 * standart base object form
 */
FormBlock::begin(['header' => $objectName]);
echo $form->field($Model, 'name')->input('text');
echo $form->field($Model, 'name_lang')->input('text');
echo $form->field($Model, 'name_short')->input('text');
echo $form->field($Model, 'description')->textarea(['rows' => 6]);
FormBlock::end();
