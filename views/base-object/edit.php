<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use system\widgets\FormAction;

$form = ActiveForm::begin();

echo $this->Template->renderPartial('@system/views/base-object/form', [
    'Model' => $Model,
    'form' => $form,
]);

FormAction::begin();
    echo Html::submitButton('Изменить', ['class' => 'btn btn-primary']);
FormAction::end();

ActiveForm::end();
