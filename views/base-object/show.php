<?php

use yii\widgets\DetailView;
use system\widgets\DetailHeader;

use system\widgets\Button;
use system\widgets\ButtonGroup;
use system\widgets\ButtonNotify;

DetailHeader::begin(['content' => $objectName]);

ButtonGroup::begin(['options' => ['class' => 'pull-right']]);
echo Button::widget(['label' => 'Редактировать', 'icon' => 'edit', 'context' => 'warning', 'action' => 'edit', 'size' => 'xs', 'url' => \Yii::$app->Route->make('edit', ['id' => $Model->id])]);
echo ButtonNotify::widget(['label' => 'Удалить', 'icon' => 'remove', 'context' => 'danger', 'action' => 'delete', 'size' => 'xs', 'url' => \Yii::$app->Route->make('delete', ['id' => $Model->id]), 'notify' => 'Удалить?']);
ButtonGroup::end();

DetailHeader::end();

echo DetailView::widget([
    'model' => $Model,
    'attributes' => [
        'name',
        'name_lang',
        'name_short',
        'description',
    ],
]);
