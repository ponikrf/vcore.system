<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->Template->setTemplate('error');
$this->Template->title = $name;

?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <p> <?= nl2br(Html::encode($message)) ?> </p>

    <h2>Возможные причины ошибки </h2>

    <ul>
        <li>Внутренняя ошибка сервера</li>
        <li>Данные были удалены</li>
        <li>Вы перешли на данную страницу по устаревшей ссылке</li>
        <li>Ошибка при генерации источника ссылки для данной страницы</li>
    </ul>
    
    <p>К сожалению, чаще всего данная проблема возникает, когда данные были удалены</p>
    
    <p><strong>Если Вы считаете, что данная проблема должна быть решена - пожалуйста, обратитесь к администратору.</strong> </p>
    
    
</div>