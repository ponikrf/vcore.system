<?php

use system\widgets\Panel;
use system\widgets\PanelBody;
use system\widgets\PanelFooter;
use system\widgets\PanelHeader;

use system\widgets\ButtonGroup;
use system\widgets\Button;

use \system\widgets\Alert;

Panel::begin();
    
    echo PanelHeader::widget(['content'=>'header']);
    
    PanelBody::begin();
           
    PanelBody::end();
    
    echo PanelFooter::widget(['content'=>'footer']);
    
Panel::end();