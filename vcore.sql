﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.3.358.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 12.04.2016 1:30:50
-- Версия сервера: 5.5.47-0ubuntu0.14.04.1
-- Версия клиента: 4.1
--


-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

-- 
-- Установка базы данных по умолчанию
--
USE vcore;

--
-- Описание для таблицы vc_application
--
DROP TABLE IF EXISTS vc_application;
CREATE TABLE vc_application (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) DEFAULT NULL COMMENT 'Название',
  name_lang VARCHAR(255) NOT NULL COMMENT 'Читаемое название',
  name_short VARCHAR(50) DEFAULT NULL COMMENT 'Короткое название',
  description TEXT DEFAULT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы vc_notify_action
--
DROP TABLE IF EXISTS vc_notify_action;
CREATE TABLE vc_notify_action (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) DEFAULT NULL COMMENT 'Название',
  name_lang VARCHAR(255) NOT NULL COMMENT 'Читаемое название',
  name_short VARCHAR(50) DEFAULT NULL COMMENT 'Короткое название',
  description TEXT DEFAULT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы vc_notify_level
--
DROP TABLE IF EXISTS vc_notify_level;
CREATE TABLE vc_notify_level (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) DEFAULT NULL COMMENT 'Название',
  name_lang VARCHAR(255) NOT NULL COMMENT 'Читаемое название',
  name_short VARCHAR(50) DEFAULT NULL COMMENT 'Короткое название',
  description TEXT DEFAULT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 6
AVG_ROW_LENGTH = 3276
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы vc_user_position
--
DROP TABLE IF EXISTS vc_user_position;
CREATE TABLE vc_user_position (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) DEFAULT NULL COMMENT 'Название',
  name_lang VARCHAR(255) NOT NULL COMMENT 'Читаемое название',
  name_short VARCHAR(50) DEFAULT NULL COMMENT 'Короткое название',
  description TEXT DEFAULT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы vc_user_role
--
DROP TABLE IF EXISTS vc_user_role;
CREATE TABLE vc_user_role (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) DEFAULT NULL COMMENT 'Название',
  name_lang VARCHAR(255) NOT NULL COMMENT 'Читаемое название',
  name_short VARCHAR(50) DEFAULT NULL COMMENT 'Короткое название',
  description TEXT DEFAULT NULL COMMENT 'Описание',
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 3
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы vc_notify
--
DROP TABLE IF EXISTS vc_notify;
CREATE TABLE vc_notify (
  id INT(11) NOT NULL AUTO_INCREMENT,
  id_user INT(11) NOT NULL DEFAULT 1000 COMMENT 'Инициатор',
  id_application INT(11) NOT NULL DEFAULT 1 COMMENT 'Приложение',
  id_notify_level INT(11) NOT NULL DEFAULT 5 COMMENT 'Важность',
  id_notify_action INT(11) NOT NULL DEFAULT 4 COMMENT 'Действие',
  notify_text VARCHAR(255) DEFAULT NULL COMMENT 'Текст',
  notify_data VARCHAR(255) DEFAULT NULL COMMENT 'Данные',
  create_datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Создано',
  PRIMARY KEY (id),
  CONSTRAINT FK_vc_notify_vc_application_id FOREIGN KEY (id_application)
    REFERENCES vc_application(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FK_vc_notify_vc_notify_action_id FOREIGN KEY (id_notify_action)
    REFERENCES vc_notify_action(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FK_vc_notify_vc_notify_level_id FOREIGN KEY (id_notify_level)
    REFERENCES vc_notify_level(id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 16
AVG_ROW_LENGTH = 1170
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы vc_user
--
DROP TABLE IF EXISTS vc_user;
CREATE TABLE vc_user (
  id INT(11) NOT NULL AUTO_INCREMENT,
  id_user_role INT(11) NOT NULL COMMENT 'Роль',
  id_user_position INT(11) NOT NULL COMMENT 'Должность',
  user_identify VARCHAR(64) NOT NULL COMMENT 'Идентификатор',
  user_password VARCHAR(32) NOT NULL COMMENT 'Пароль',
  user_name VARCHAR(120) NOT NULL COMMENT 'Имя пользователя',
  user_locale VARCHAR(10) NOT NULL DEFAULT 'ru_RU' COMMENT 'Локализация',
  start_datetime DATETIME DEFAULT NULL COMMENT 'Дата начала',
  end_datetime DATETIME DEFAULT NULL COMMENT 'Дата конца',
  create_datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Создано',
  PRIMARY KEY (id),
  UNIQUE INDEX user_identify (user_identify),
  CONSTRAINT FK_vc_user_vc_user_position_id FOREIGN KEY (id_user_position)
    REFERENCES vc_user_position(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FK_vc_user_vc_user_role_id FOREIGN KEY (id_user_role)
    REFERENCES vc_user_role(id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 1002
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Описание для таблицы vc_user_permission
--
DROP TABLE IF EXISTS vc_user_permission;
CREATE TABLE vc_user_permission (
  id INT(11) NOT NULL AUTO_INCREMENT,
  id_user INT(11) NOT NULL DEFAULT 1000 COMMENT 'Инициатор',
  id_user_role INT(11) NOT NULL DEFAULT 1 COMMENT 'Роль',
  module VARCHAR(255) NOT NULL COMMENT 'Модуль',
  entity VARCHAR(255) NOT NULL COMMENT 'Сущность',
  action VARCHAR(255) NOT NULL COMMENT 'Действие',
  create_datetime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Создано',
  PRIMARY KEY (id),
  CONSTRAINT FK_vc_user_permission_vc_user_id FOREIGN KEY (id_user)
    REFERENCES vc_user(id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT FK_vc_user_permission_vc_user_role_id FOREIGN KEY (id_user_role)
    REFERENCES vc_user_role(id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 51
AVG_ROW_LENGTH = 963
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- 
-- Вывод данных для таблицы vc_application
--
INSERT INTO vc_application VALUES
(1, 'VCore', 'VCore', NULL, NULL);

-- 
-- Вывод данных для таблицы vc_notify_action
--
INSERT INTO vc_notify_action VALUES
(1, 'insert', 'Добавление', NULL, NULL),
(2, 'update', 'Обновление', NULL, NULL),
(3, 'delete', 'Удаление', NULL, NULL),
(4, 'info', 'Информирование', NULL, NULL);

-- 
-- Вывод данных для таблицы vc_notify_level
--
INSERT INTO vc_notify_level VALUES
(1, 'error', 'Ошибка', NULL, NULL),
(2, 'warning', 'Предупреждение', NULL, NULL),
(3, 'info', 'Оповещение', NULL, NULL),
(4, 'message', 'Сообщение', NULL, NULL),
(5, 'system', 'Системное', NULL, NULL);

-- 
-- Вывод данных для таблицы vc_user_position
--
INSERT INTO vc_user_position VALUES
(1, NULL, 'Администрация', NULL, NULL);

-- 
-- Вывод данных для таблицы vc_user_role
--
INSERT INTO vc_user_role VALUES
(1, 'system', 'Система', NULL, NULL),
(2, 'administator', 'Администратор', NULL, NULL);

-- 
-- Вывод данных для таблицы vc_notify
--
INSERT INTO vc_notify VALUES
(2, 1000, 1, 1, 4, 'Incorrect username or password', '{"identify":"admin"}', '2016-04-10 19:00:19'),
(3, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-10 19:06:08'),
(4, 1000, 1, 3, 4, 'Logout user', '{"id":1000}', '2016-04-10 21:08:00'),
(5, 1000, 1, 1, 4, 'Incorrect username or password', '{"identify":"admin"}', '2016-04-10 21:08:02'),
(6, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-10 21:08:06'),
(7, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-10 22:13:27'),
(8, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-11 00:47:17'),
(9, 1000, 1, 3, 4, 'Logout user', '{"id":1000}', '2016-04-11 00:54:21'),
(10, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-11 00:54:22'),
(11, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-11 14:43:25'),
(12, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-11 16:21:57'),
(13, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-11 17:56:21'),
(14, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-11 19:53:54'),
(15, 1001, 1, 3, 4, 'Login success', '{"identify":"admin"}', '2016-04-11 23:10:37');

-- 
-- Вывод данных для таблицы vc_user
--
INSERT INTO vc_user VALUES
(1000, 1, 1, 'system', 'a578ab2663114be4964e60fe828d0f60', 'Система', 'ru_RU', '2016-01-01 00:00:00', NULL, '2016-04-10 21:09:03'),
(1001, 2, 1, 'admin', 'fed009fcde5df616dfec5e15c2215765', 'Администратор', 'ru_RU', '2016-01-01 00:00:00', NULL, '2016-04-10 19:15:38');

-- 
-- Вывод данных для таблицы vc_user_permission
--
INSERT INTO vc_user_permission VALUES
(22, 1000, 2, 'system', 'base_table', 'add', '2016-04-10 20:57:03'),
(23, 1000, 2, 'system', 'base_table', 'update', '2016-04-10 20:57:03'),
(24, 1000, 2, 'system', 'user', 'show', '2016-04-10 20:57:03'),
(25, 1000, 2, 'system', 'user', 'add', '2016-04-10 20:57:03'),
(26, 1000, 2, 'system', 'user', 'edit', '2016-04-10 20:57:03'),
(27, 1000, 2, 'system', 'user', 'close', '2016-04-10 20:57:03'),
(28, 1000, 2, 'system', 'user', 'change_password', '2016-04-10 20:57:03'),
(29, 1000, 2, 'system', 'user_role', 'show', '2016-04-10 20:57:04'),
(30, 1000, 2, 'system', 'user_role', 'add', '2016-04-10 20:57:04'),
(31, 1000, 2, 'system', 'user_role', 'edit', '2016-04-10 20:57:04'),
(32, 1000, 2, 'system', 'user_role', 'delete', '2016-04-10 20:57:04'),
(33, 1000, 2, 'system', 'user_position', 'show', '2016-04-10 20:57:04'),
(34, 1000, 2, 'system', 'user_position', 'add', '2016-04-10 20:57:04'),
(35, 1000, 2, 'system', 'user_position', 'edit', '2016-04-10 20:57:04'),
(36, 1000, 2, 'system', 'user_position', 'delete', '2016-04-10 20:57:04'),
(37, 1000, 2, 'system', 'user_permission', 'show', '2016-04-10 20:57:04'),
(38, 1000, 2, 'system', 'user_permission', 'edit', '2016-04-10 20:57:04');

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;