<?php

use system\widgets\Panel;
use system\widgets\PanelBody;
use system\widgets\PanelFooter;
use system\widgets\PanelHeader;

/*
 * Context type
 * 
 * default - grey
 * primary - blue
 * success - green
 * info - white blue
 * warning - yellow
 * danger - red
 * link - no formatted
 */

Panel::begin();
    
    echo PanelHeader::widget(['content'=>'header']);
    
    PanelBody::begin();
        echo "This is panel";
    PanelBody::end();
    
    echo PanelFooter::widget(['content'=>'footer']);
    
Panel::end();